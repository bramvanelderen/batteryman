﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DemoCameraTargetInjection : MonoBehaviour {

	// Use this for initialization
	void Start () {
        var player = GameObject.FindGameObjectWithTag("Player");
        GetComponent<IsometricCamera>().AddTarget(player.transform);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
