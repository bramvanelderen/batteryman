﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetPlayerInvunerable : MonoBehaviour {

    [SerializeField] PlayerController _player;

	// Use this for initialization
	void Start () {
        if (_player != null)
            _player.invunerable = true;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
