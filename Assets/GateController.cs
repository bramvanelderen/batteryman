﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GateController : LevelComponent {

    public Level ReactToLevel;
    public bool CloseAfterPrepare = true;
    public bool CloseAfterStart = false;
    public bool OpenAfterEnd = true;
    private Animator _anim;

    private void Start()
    {
        _anim = GetComponent<Animator>();

        if (ReactToLevel != null)
            ReactToLevel.Components.Add(this);

    }

    public override void OnLevelEnd()
    {
        if (OpenAfterEnd)
        {
            _anim.SetBool("IsClosed", false);
        }
    }

    public override void OnRoundBegin()
    {
        if (CloseAfterStart)
            _anim.SetBool("IsClosed", true);
    }

    public override void OnRoundPrepare()
    {
        if (CloseAfterPrepare)
            _anim.SetBool("IsClosed", true);
    }
}
