using UnityEngine;
using System.Collections;
 
public class PhysicalExplosion : MonoBehaviour 
{
    public float radius = 7.6f;// explosion radius
    
    void Update () 
    {
        
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, radius);// create explosion
        
        for(int i=0; i<hitColliders.Length; i++)
        {              
            if(hitColliders[i].CompareTag("Enemy"))
            {
                //destroy enemies in radius
                //if it's an explode enemy type, trigger a chain explosion
                if (hitColliders[i].gameObject.GetComponent<Explode>())
                {
                    StartCoroutine(DelayedExplosion(hitColliders[i].gameObject));
//                    print("CHAIN EXPLOSION");
                }
                else
                {
//                    print("EXPLOSION KILLED NEARBY ENEMY: "+hitColliders[i].gameObject.name);
                    Destroy(hitColliders[i].gameObject);
                }
            }
			
        }
        Destroy(gameObject,0.35f);// destroy explosion
    }
    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position,radius);
    }

    IEnumerator DelayedExplosion(GameObject go)
    {
        yield return new WaitForSeconds(0.25f);
//        print("DELAYED EXPLOSION");
        go.GetComponent<Explode>().Explosion();
    }
}