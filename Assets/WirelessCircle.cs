﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WirelessCircle : MonoBehaviour
{

	private AudioSource audioSource;
	public float startPitch = 0.3f;
	public float endPitch = 1;

	// Use this for initialization
	void Start ()
	{
		audioSource = GetComponent<AudioSource>();
		audioSource.pitch = startPitch;
	}
	
	// Update is called once per frame
	void Update ()
	{
		audioSource.pitch = transform.localScale.x;
	}
}
