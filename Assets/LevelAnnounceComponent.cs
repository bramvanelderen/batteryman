﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelAnnounceComponent : LevelComponent {

    public string messageOnPrepare;

    public string messageOnBegin;
    public EnemyType Type;
    public float Duration;

    public override void OnLevelEnd()
    {

    }

    public override void OnRoundBegin()
    {

    }

    public override void OnRoundPrepare()
    {
        FactoryAnnouncement.GlobalAnnounce(messageOnPrepare, Duration);
    }
}
