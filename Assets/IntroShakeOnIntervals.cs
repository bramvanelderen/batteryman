﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntroShakeOnIntervals : MonoBehaviour
{

	private CameraShake cameraShake;
	
	// Use this for initialization
	void Start ()
	{
		cameraShake = GetComponent<CameraShake>();
		StartCoroutine(ShakeOnInterval());
	}

	IEnumerator ShakeOnInterval()
	{
		while (true)
		{
			yield return new WaitForSeconds(3f);
			cameraShake.shakeDuration = 10f;
		}
	}
}
