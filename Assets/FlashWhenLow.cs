﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FlashWhenLow : MonoBehaviour {

    public GameObject objectToFlash;
    public GameObject backgroundToFlash;
    public AudioClip oneLinerClip;
    public float lowThreshold = 0.19f;

    Image img;
    Image bgimg;
    Color initCol, initBgCol;
    float initX, initY, initZ;

    bool shaking = false, flashing = false, playedOneLiner = false;
    private AudioSource audioSource;

	// Use this for initialization
	void Start () {
        initX = transform.position.x;
        initY = transform.position.y;
        initZ = transform.position.z;
	    
        if(objectToFlash == null || backgroundToFlash == null)
        {
            Debug.LogWarning("Object or background to Flash is empty");
        }
        else
        {
            img = objectToFlash.GetComponent<Image>();
            bgimg = backgroundToFlash.GetComponent<Image>();
            initCol = img.color;
            initBgCol = bgimg.color;
        }

	    audioSource = GetComponent<AudioSource>();
	}

    // Update is called once per frame
    void Update () {
		if(img.fillAmount < (1 * lowThreshold))
        {
            if (!playedOneLiner)
            {
                playedOneLiner = true;
                audioSource.volume = 1;
                audioSource.PlayOneShot(oneLinerClip);
            }
            else if (!audioSource.isPlaying)
            {
                audioSource.volume = 0.3f;
                audioSource.Play();
            }
            if (!shaking)
            {
                shaking = true;
                StartCoroutine(Shake());
            }
            //if (!flashing)
            //{
            //    flashing = true;
            //    StartCoroutine(Flash());
            //}
        } else if (shaking || flashing || GetComponent<AudioSource>().isPlaying)
        {
            audioSource.Stop();
            StopAllCoroutines();
            RestoreState();
        } else if (playedOneLiner)
        {
            playedOneLiner = false;
        }
	}

    private void RestoreState()
    {
        shaking = flashing = false;
        img.color = initCol;
        bgimg.color = initBgCol;
        transform.position = new Vector3(initX, initY, initZ);
    }

    IEnumerator Flash()
    {
        for (int i = 0; i < 4; i++)
        {
            Color newCol = img.color;
            newCol.a = (newCol.a == 0) ? 255 : 0;
            img.color = newCol;
            Color newBgCol = bgimg.color;
            newBgCol.a = (newBgCol.a == 0) ? 255 : 0;
            bgimg.color = newBgCol;
            yield return new WaitForSeconds(0.3f);
        }
        img.color = initCol;
        bgimg.color = initBgCol;
        flashing = false;
    }


    IEnumerator Shake()
    {

        for (int i = 0; i < 10; i++)
        {
            Vector3 newpos = new Vector3(initX + (Random.value - 0.5f)*3, initY + (Random.value - 0.5f)*3, initZ);
            transform.position = newpos;
            yield return new WaitForSeconds(0.1f);
        }
        transform.position = new Vector3(initX, initY, initZ);
        shaking = false;
    }
}
