﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReactiveLight : MonoBehaviour {

    [SerializeField] private Light _light;

    private float _baseIntensity;

	// Use this for initialization
	void Start () {
        _baseIntensity = _light.intensity;
        GlobalPower.OnPowerChangeEvent.AddListener(UpdateIntensity);
	}

    void UpdateIntensity(float value)
    {
        _light.intensity = _baseIntensity * value;
    }

    private void OnDestroy()
    {
        GlobalPower.OnPowerChangeEvent.RemoveListener(UpdateIntensity);
    }
}
