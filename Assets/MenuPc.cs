﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuPc : MonoBehaviour
{
    [SerializeField] CustomClip titleTrack;
    [SerializeField] CustomClip voiceTrack;


    // Use this for initialization
    void Start()
    {
        foreach (var obj in GameObject.FindObjectsOfType<MenuPc>())
        {
            if (obj != this)
                Destroy(obj.gameObject);
        }

        titleTrack.Play(CustomClip.CreateAudioSource());
        voiceTrack.Play(CustomClip.CreateAudioSource());
    }

    public void StartGame()
    {
        SceneManager.LoadScene("MainLevel");
    }

    public void ExitGame()
    {
        //If we are running in a standalone build of the game
    #if UNITY_STANDALONE //Quit the application
		Application.Quit();
	#endif

        //If we are running in the editor
    #if UNITY_EDITOR
        //Stop playing the scene
        UnityEditor.EditorApplication.isPlaying = false;
    #endif
    }
}