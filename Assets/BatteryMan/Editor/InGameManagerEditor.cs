﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;

[CustomEditor(typeof(InGameManager))]
public class InGameManagerEditor : Editor {

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        var script = (InGameManager)target;

        GUILayout.Space(10);
        GUILayout.Label("In game stuff");
        if (GUILayout.Button("Start Game"))
        {
            script.StartGame();
        }
    }

    void IsFinish()
    {
        Debug.Log("Level Finished callback recieved");
    }
}
