﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(FactoryAnnouncement))]
public class FactoryAnnouncementEditor : Editor {

    private string _text;
    private float _duration;
    private EnemyType _type;

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        var script = (FactoryAnnouncement)target;


        _text = EditorGUILayout.TextField(_text);
        _duration = EditorGUILayout.Slider(_duration, 1, 10);
        _type = (EnemyType)EditorGUILayout.EnumPopup(_type);

        if (GUILayout.Button("ShowMessage"))
            script.Announce(_text, _type, _duration);
    }
}
