﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(GeneratorFx))]
public class GeneratorFxEditor : Editor {
    private float _value;

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        var script = (GeneratorFx)target;

        _value = EditorGUILayout.Slider(_value, 0, 1);
        if (GUILayout.Button("Update Fx"))
        {
            script.T = _value;
        }
    }
}
