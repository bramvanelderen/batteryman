﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;

[CustomEditor(typeof(Level))]
public class LevelEditor : Editor {
    private int _value;

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        var script = (Level)target;
        GUILayout.Space(10);
        GUILayout.Label("In game stuff");
        _value = EditorGUILayout.IntField("Round", _value);
        if (GUILayout.Button("Start Level"))
        {
            script.StartLevel(_value);
        }
    }

    void IsFinish()
    {
        Debug.Log("Level Finished callback recieved");
    }
}
