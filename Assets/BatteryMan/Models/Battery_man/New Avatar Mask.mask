%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_PrefabParentObject: {fileID: 0}
  m_PrefabInternal: {fileID: 0}
  m_Name: New Avatar Mask
  m_Mask: 01000000000000000000000000000000000000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: battery_man
    m_Weight: 1
  - m_Path: SeekerTureRedio
    m_Weight: 1
  - m_Path: SeekerTureRedio/arm.pole.l
    m_Weight: 1
  - m_Path: SeekerTureRedio/arm.pole.l/arm.pole.l_end
    m_Weight: 1
  - m_Path: SeekerTureRedio/arm.pole.r
    m_Weight: 1
  - m_Path: SeekerTureRedio/arm.pole.r/arm.pole.r_end
    m_Weight: 1
  - m_Path: SeekerTureRedio/arm.target.l
    m_Weight: 1
  - m_Path: SeekerTureRedio/arm.target.l/arm.target.l_end
    m_Weight: 1
  - m_Path: SeekerTureRedio/arm.target.r
    m_Weight: 1
  - m_Path: SeekerTureRedio/arm.target.r/arm.target.r_end
    m_Weight: 1
  - m_Path: SeekerTureRedio/Hips
    m_Weight: 0
  - m_Path: SeekerTureRedio/Hips/Spine
    m_Weight: 0
  - m_Path: SeekerTureRedio/Hips/Spine/Chest
    m_Weight: 0
  - m_Path: SeekerTureRedio/Hips/Spine/Chest/UpperChest
    m_Weight: 0
  - m_Path: SeekerTureRedio/Hips/Spine/Chest/UpperChest/Neck
    m_Weight: 0
  - m_Path: SeekerTureRedio/Hips/Spine/Chest/UpperChest/Neck/Head
    m_Weight: 0
  - m_Path: SeekerTureRedio/Hips/Spine/Chest/UpperChest/Neck/Head/Head_end
    m_Weight: 0
  - m_Path: SeekerTureRedio/Hips/Spine/Chest/UpperChest/Shoulder.l
    m_Weight: 1
  - m_Path: SeekerTureRedio/Hips/Spine/Chest/UpperChest/Shoulder.l/UpperArm.l
    m_Weight: 1
  - m_Path: SeekerTureRedio/Hips/Spine/Chest/UpperChest/Shoulder.l/UpperArm.l/LowerArm.l
    m_Weight: 1
  - m_Path: SeekerTureRedio/Hips/Spine/Chest/UpperChest/Shoulder.l/UpperArm.l/LowerArm.l/Hand.l
    m_Weight: 1
  - m_Path: SeekerTureRedio/Hips/Spine/Chest/UpperChest/Shoulder.l/UpperArm.l/LowerArm.l/Hand.l/Hand.l_end
    m_Weight: 1
  - m_Path: SeekerTureRedio/Hips/Spine/Chest/UpperChest/Shoulder.r
    m_Weight: 1
  - m_Path: SeekerTureRedio/Hips/Spine/Chest/UpperChest/Shoulder.r/UpperArm.r
    m_Weight: 1
  - m_Path: SeekerTureRedio/Hips/Spine/Chest/UpperChest/Shoulder.r/UpperArm.r/LowerArm.r
    m_Weight: 1
  - m_Path: SeekerTureRedio/Hips/Spine/Chest/UpperChest/Shoulder.r/UpperArm.r/LowerArm.r/Hand.r
    m_Weight: 1
  - m_Path: SeekerTureRedio/Hips/Spine/Chest/UpperChest/Shoulder.r/UpperArm.r/LowerArm.r/Hand.r/Hand.r_end
    m_Weight: 1
  - m_Path: SeekerTureRedio/Hips/UpperLeg.l
    m_Weight: 0
  - m_Path: SeekerTureRedio/Hips/UpperLeg.l/LowerLeg.l
    m_Weight: 0
  - m_Path: SeekerTureRedio/Hips/UpperLeg.l/LowerLeg.l/Foot.l
    m_Weight: 0
  - m_Path: SeekerTureRedio/Hips/UpperLeg.l/LowerLeg.l/Foot.l/Foot.l_end
    m_Weight: 0
  - m_Path: SeekerTureRedio/Hips/UpperLeg.r
    m_Weight: 0
  - m_Path: SeekerTureRedio/Hips/UpperLeg.r/LowerLeg.r
    m_Weight: 0
  - m_Path: SeekerTureRedio/Hips/UpperLeg.r/LowerLeg.r/Foot.r
    m_Weight: 0
  - m_Path: SeekerTureRedio/Hips/UpperLeg.r/LowerLeg.r/Foot.r/Foot.r_end
    m_Weight: 0
  - m_Path: SeekerTureRedio/Leg.Control.l
    m_Weight: 0
  - m_Path: SeekerTureRedio/Leg.Control.l/leg.pole.l
    m_Weight: 0
  - m_Path: SeekerTureRedio/Leg.Control.l/leg.pole.l/leg.pole.l_end
    m_Weight: 0
  - m_Path: SeekerTureRedio/Leg.Control.r
    m_Weight: 0
  - m_Path: SeekerTureRedio/Leg.Control.r/leg.pole.r
    m_Weight: 0
  - m_Path: SeekerTureRedio/Leg.Control.r/leg.pole.r/leg.pole.r_end
    m_Weight: 0
  - m_Path: SeekerTureRedio/leg.target.l
    m_Weight: 0
  - m_Path: SeekerTureRedio/leg.target.l/leg.target.l_end
    m_Weight: 0
  - m_Path: SeekerTureRedio/leg.target.r
    m_Weight: 0
  - m_Path: SeekerTureRedio/leg.target.r/leg.target.r_end
    m_Weight: 0
  - m_Path: SeekerTureRedio/Root
    m_Weight: 0
  - m_Path: SeekerTureRedio/Root/Root_end
    m_Weight: 0
