﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneratorFx : MonoBehaviour {

    [SerializeField] private Color _colorMinCharging;
    [SerializeField] private Color _colorMaxCharging;
    [SerializeField] private Color _colorMinSpinning;
    [SerializeField] private Color _colorTick;
    [SerializeField] private Color _colorMaxSpinning;
    public float _emissionMin;
    public float _emissionMax;

    [SerializeField] private MeshRenderer _baseMesh;
    [SerializeField] private MeshRenderer _topMesh;

    [SerializeField] private Material baseMat;
    [SerializeField] private Material blankMat;
    [SerializeField] private Material electricMat;
    [SerializeField] private Material tickMat;
    [SerializeField] private Material spinMat;

    private Material _baseMat;
    private Material _topMatElectric;
    private Material _topMatSpin;
    private Material _topMatBlank;
    private Material _topMatTick;
    private Color _colorMin;
    private Color _colorMax;
    private float _t;

    public float T {
        get
        {
            return _t;
        }
        set
        {
            _t = value;
            var emission = Mathf.Lerp(_emissionMin, _emissionMax, _t);
            
            Color color = Color.Lerp(_colorMin, _colorMax, _t);
            
            UpdateMaterials(emission, color, _baseMat, _topMatElectric, _topMatSpin, _topMatBlank, _topMatTick);

        }
    }
    
    public void SwitchToBlankMatDisabled()
    {
        _topMesh.material = _topMatBlank;
        _colorMin = Color.black;
        _colorMax = Color.black;
        T = 0;
    }
    
    public void SwitchToBlankMatSpinning()
    {
        _topMesh.material = _topMatBlank;
        _colorMin = _colorMinSpinning;
        _colorMax = _colorMinSpinning;
        T = 0;
    }

    public void SwitchToSpinMat()
    {
        _topMesh.material = _topMatSpin;
        _colorMin = _colorMinSpinning;
        _colorMax = _colorMaxSpinning;
        T = 0;
    }
    
    public void SwitchToElectricMat()
    {
        _topMesh.material = _topMatElectric;
        _colorMin = _colorMinCharging;
        _colorMax = _colorMaxCharging;
        T = 0;
    }
    
    public void SwitchToTickMat()
    {
        _topMesh.material = _topMatTick;
        _colorMin = _colorTick;
        _colorMax = _colorTick;
        T = 1;
    }

    void UpdateMaterials(float emission, Color color, params Material[] materials)
    {
        foreach(var mat in materials)
        {
            mat.SetColor("_EmissionColor", color * emission);
        }
    }

    // Use this for initialization
    void Awake () {
        _baseMat = Instantiate(baseMat);
        _topMatElectric = Instantiate(electricMat);
        _topMatSpin = Instantiate(spinMat);
        _topMatBlank = Instantiate(blankMat);
        _topMatTick = Instantiate(tickMat);
        _baseMesh.material = _baseMat;
        
        SwitchToElectricMat();
        
        T = 0;
    }    
}
