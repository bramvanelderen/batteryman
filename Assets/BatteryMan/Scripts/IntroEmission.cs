﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntroEmission : MonoBehaviour
{
	
	public Light worldLight;

	private AudioSource powerDownSound;
	private AudioSource uhOhSound;
	private AudioSource electrocutionSound;
	private AudioSource music;
	
	private Color blueEmissionColor;
	private GameObject playerModel;
	private CameraShake cameraShake;
	private bool entered;

	// Use this for initialization
	void Start () {
        Cursor.lockState = CursorLockMode.Confined;
		playerModel = GameObject.Find("batteryman");
		blueEmissionColor = playerModel.GetComponentInChildren<Renderer>().material.GetColor("_EmissionColor");
		playerModel.GetComponentInChildren<Renderer>().material.SetColor("_EmissionColor", Color.red);
		cameraShake = GameObject.Find("MainCamera").GetComponent<CameraShake>();
		powerDownSound = GetComponents<AudioSource>()[0];
		uhOhSound = GetComponents<AudioSource>()[1];
		electrocutionSound = GetComponents<AudioSource>()[2];
		music = GetComponents<AudioSource>()[4];
		entered = false;
		
		StartCoroutine(ShakeOnInterval());
	}

	private void OnCollisionEnter(Collision other)
	{
		if (other.collider.CompareTag("Generator") && !entered)
		{
			entered = true;
			playerModel.GetComponentInChildren<Renderer>().material.SetColor("_EmissionColor", blueEmissionColor);
			GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
			StopAllCoroutines();
			cameraShake.shakeDuration = 4f;
			music.Stop();
			worldLight.color = Color.white;
			electrocutionSound.Play();
			StartCoroutine(RandomPoses());
		}
	}

	IEnumerator ShakeOnInterval()
	{
		while (true)
		{
			yield return new WaitForSeconds(2.5f);
			cameraShake.shakeDuration = 0.4f;
		}
	}

	IEnumerator RandomPoses()
	{
		transform.position = new Vector3(transform.position.x, 3f, transform.position.z - 1f);
		transform.eulerAngles = new Vector3(Random.Range(0f,359f), Random.Range(0f,359f), Random.Range(0f,359f));
		yield return new WaitForSeconds(0.5f);
		transform.eulerAngles = new Vector3(Random.Range(0f,359f), Random.Range(0f,359f), Random.Range(0f,359f));
		yield return new WaitForSeconds(0.5f);
		transform.eulerAngles = new Vector3(Random.Range(0f,359f), Random.Range(0f,359f), Random.Range(0f,359f));
		yield return new WaitForSeconds(0.5f);
		transform.eulerAngles = new Vector3(Random.Range(0f,359f), Random.Range(0f,359f), Random.Range(0f,359f));
		yield return new WaitForSeconds(0.5f);
		transform.eulerAngles = new Vector3(Random.Range(0f,359f), Random.Range(0f,359f), Random.Range(0f,359f));
		yield return new WaitForSeconds(0.5f);
		transform.eulerAngles = new Vector3(Random.Range(0f,359f), Random.Range(0f,359f), Random.Range(0f,359f));
		yield return new WaitForSeconds(0.5f);
		transform.eulerAngles = new Vector3(Random.Range(0f,359f), Random.Range(0f,359f), Random.Range(0f,359f));
		yield return new WaitForSeconds(0.5f);
		transform.eulerAngles = new Vector3(Random.Range(0f,359f), Random.Range(0f,359f), Random.Range(0f,359f));
		yield return new WaitForSeconds(0.5f);
		transform.position = new Vector3(transform.position.x, 0, transform.position.z);
		transform.eulerAngles = new Vector3(0, 180, 0);
		StartCoroutine(Electrocuted());
	}
	
	IEnumerator Electrocuted()
	{
		GeneratorFx gfx = GameObject.Find("Generator").GetComponent<GeneratorFx>();
		gfx._emissionMin = 0;
		gfx._emissionMax = 0;
		gfx.T = 0;
		powerDownSound.Play();
		worldLight.enabled = false;
		yield return new WaitUntil(() => !powerDownSound.isPlaying);
		yield return new WaitForSeconds(1f);
		uhOhSound.Play();
		yield return new WaitUntil(() => !uhOhSound.isPlaying);

        Initiate.Fade("TitleScreenDynamic", Color.black, 1);
    }
}
