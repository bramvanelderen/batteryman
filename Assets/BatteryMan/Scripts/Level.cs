﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Random = UnityEngine.Random;
using UnityEngine.Events;

public class Level : MonoBehaviour
{
    public int Index;
    public List<LevelComponent> Components;

    [SerializeField] private Transform _respawnPoint;
    [SerializeField] private List<RoundData> _rounds;
    [SerializeField] private List<GeneratorController> _generators;
    [SerializeField] private List<Transform> _enemySpawns;
    [SerializeField] private List<OnTriggerEventHandler> _levelTriggers;
    [SerializeField] private bool _unlimitedRounds;

    private float _spawnTime = 0f;
    private bool _inProgress;
    private bool _waitingForTrigger = true;
    private PlayerController _player;
    private List<GameObject> _cleanUpList;

    /// <summary>
    /// Use the round data to see how many generators are activated this round and how many in total have to be activated
    /// Be carefull this variable is NULL when round isn't ongoing
    /// </summary>
    public RoundData Round { get; private set; }    

    private Vector3 RandomPosition
    {
        get
        {
            if (_enemySpawns == null || _enemySpawns.Count == 0)
                return Vector3.zero + Vector3.up;

            var spawn = _enemySpawns[Random.Range(0, _enemySpawns.Count)];

            return spawn.position + Vector3.up;
        }
    }

    public bool IsInProgress
    {
        get
        {
            return _inProgress;
        }
    }

    public int FinalRoundId
    {
        get
        {
            if (_unlimitedRounds)
                return 9999;

            return _rounds.Aggregate((x1, x2) => (x1.RoundId > x2.RoundId) ? x1 : x2).RoundId;
        }
    }

    public UnityEvent OnGeneratorActiveEvent { get; private set; }

    void Start()
    {
        if (OnGeneratorActiveEvent == null)
            OnGeneratorActiveEvent = new UnityEvent();

        LevelManager.AddLevel(this);
        _inProgress = false;
        _cleanUpList = new List<GameObject>();

        if (Components == null)
            Components = new List<LevelComponent>();

        foreach (var comp in GetComponents<LevelComponent>())
            Components.Add(comp);

        foreach (var triggerListener in _levelTriggers)
        {
            triggerListener.OnPlayerEvent.AddListener(StartRound);
        }
    }

    public bool StartLevel(int round)
    {
        LevelManager.CanStartLevel();
        foreach (var gen in _generators)
        {
            gen.OnActivateEvent.RemoveAllListeners();
        }

        Round = _rounds.Find(x=> x.RoundId == round);
        if (Round == null && _unlimitedRounds)
            Round = _rounds.Last();
        if (Round == null)
            return false;

        Round.ResetCounters();
        _inProgress = true;
        _waitingForTrigger = true;

        foreach (var comp in Components)
            comp.OnRoundPrepare();

        LevelManager.OnRoundPrepare.Invoke(this);
        FactoryAnnouncement.GlobalAnnounce(Round.MessageOnRoundPrepare, 30);
//        print("Level" + Index + " waiting for trigger");

        return true;
    }

    void StartRound(GameObject player)
    {
        if (!IsInProgress || !_waitingForTrigger)
            return;

        List<GeneratorController> toActivate = new List<GeneratorController>(_generators);
        for (int i = 0; i < Round.GeneratorAmount; i++)
        {
            if (toActivate.Count == 0)
                break;

            var gen = toActivate[Random.Range(0, toActivate.Count)];
            gen.Enable();
            gen.OnActivateEvent.AddListener(OnActivateGeneratorListener);
            toActivate.Remove(gen);
        }
        _player = player.GetComponent<PlayerController>();
        _player.invunerable = false;
        _player.Power = _player.maxPower;
        _player.isGameOver = false;
        _player.OnOutOfPower.AddListener(OutOfPowerListener);
        GlobalPower.PowerLevel = 0;

        foreach (var comp in Components)
            comp.OnRoundBegin();

//        print("Level" + Index + " activated");
        FactoryAnnouncement.GlobalAnnounce(Round.MessageOnRoundStart, 30);

        LevelManager.OnRoundStart.Invoke(this);

        _waitingForTrigger = false;
    }

    void OnActivateGeneratorListener()
    {
        if (!_inProgress || _waitingForTrigger)
            return;

        Round.ActivatedGenerators++;
        OnGeneratorActiveEvent.Invoke();
//        print("Generator activated");

        GlobalPower.PowerLevel =  (float)Round.ActivatedGenerators / (float)Round.GeneratorAmount;

        if (Round.ActivatedGenerators >= Round.GeneratorAmount)
            StopLevel();
    }

    public void OutOfPowerListener()
    {
        if (!_inProgress || _waitingForTrigger)
            return;

//        print("Player ran out of power");
        LevelManager.OnRoundFailed.Invoke(this);

        _player.OnOutOfPower.RemoveListener(OutOfPowerListener);

        if (_respawnPoint != null)
        {
            _player.transform.position = _respawnPoint.position + Vector3.up;
        }

        CleanLevel();
        foreach (var gen in _generators)
        {
            gen.Disable();
        }

        _inProgress = false;
        StartLevel(Round.RoundId);
        StartRound(_player.gameObject);


    }

    public void StopLevel()
    {
        

        GlobalPower.PowerLevel = 1;

        _player.invunerable = true;
        _player.OnOutOfPower.RemoveListener(OutOfPowerListener);

        foreach (var comp in Components)
            comp.OnLevelEnd();

        _inProgress = false;
        Round = null;
        CleanLevel();

        LevelManager.OnRoundEnd.Invoke(this);
//        print("Level " + Index + " is finished");

    }

    void CleanLevel()
    {
        foreach ( var obj in _cleanUpList)
        {
            Destroy(obj);
        }
        _cleanUpList.Clear();

        foreach ( var gen in _generators)
        {
            gen.Disable();
        }
    }

    private void Update()
    {
        if (!_inProgress || _waitingForTrigger)
            return;

        if (Time.time > _spawnTime)
        {
            var amountToSpawn = Round.SpawnAmount.Random;
            for (int i = 0; i < amountToSpawn; i++)
            {
                var prefab = Round.WhatToSpawn();
                if (prefab != null)
                {
                    var obj = Instantiate(prefab);
                    obj.transform.position = RandomPosition;
                    _cleanUpList.Add(obj);
                }
            }

            _spawnTime = Time.time + Round.SpawnInterval.Random;
        }
        


    }
    private void OnDestroy()
    {
        LevelManager.RemoveLevel(this);
    }
}

[System.Serializable]
public class RoundData
{
    public int RoundId;
    public int GeneratorAmount;
    public Range SpawnInterval;
    public Range SpawnAmount;
    public List<Enemy12> EnemiesToSpawn;
    public string MessageOnRoundPrepare;
    public string MessageOnRoundStart;

    public int ActivatedGenerators { get; set; }

    public void ResetCounters()
    {
        foreach (var enemy in EnemiesToSpawn)
        {
            enemy.TimesSpawned = 0;
        }
        ActivatedGenerators = 0;
    }

    public GameObject WhatToSpawn()
    {
        if (EnemiesToSpawn.Count == 0)
            return null;
        var lowestEnemies = EnemiesToSpawn.Aggregate((x1, x2) => (x1.TimesSpawned < x2.TimesSpawned ? x1 : x2));
        if (lowestEnemies.TimesSpawned > lowestEnemies.SpawnCount)
            return null;

        lowestEnemies.TimesSpawned++;
        return EnemyData.GetEnemyPrefab(lowestEnemies.type);
    }

}

[System.Serializable]
public class Enemy12
{
    public EnemyType type;
    public int SpawnCount;
    
    public int TimesSpawned { get; set; }

}



