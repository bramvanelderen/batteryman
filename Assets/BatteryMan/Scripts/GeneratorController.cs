﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[RequireComponent(typeof(GeneratorFx))]
public class GeneratorController : MonoBehaviour, Interactable
{
    public float requiredPower = 10;
    [HideInInspector] public float power;
    
    public AudioClip chargingClip;
    public AudioClip activatedClip;
    public AudioClip movingDownClip;
    private AudioSource chargingAudioSource;
    private AudioSource activatedAudioSource;
    private AudioSource movingDownAudioSource;
    
    private GeneratorFx fx;
    private CapsuleCollider capsuleCollider;
    private float timeToStopPlayingChargeAudio = 0;

    private SpinDetection spin;

    public bool isEnabled = false;
    public bool isFullyCharged = false;
    public bool isDone = false; //done after fully charging and fully spinning
    public bool isReadyToSpin = false;

    public UnityEvent OnActivateEvent;

    public float Power
    {
        get { return power; }
        set
        {
            if (isFullyCharged) return;

            if (value > 0)
            {
                if (!chargingAudioSource.isPlaying)
                {
                    chargingAudioSource.Play();
                }
                chargingAudioSource.pitch = 1 + (value / requiredPower)*2;
                timeToStopPlayingChargeAudio = Time.time + 0.05f;
            }

            power = value;

            if (fx != null)
                fx.T = Mathf.Clamp01(power / requiredPower);

            if (power >= requiredPower)
            {
                power = requiredPower;
                chargingAudioSource.Stop();
                activatedAudioSource.PlayOneShot(activatedClip);
                isFullyCharged = true;
                StopAllCoroutines();
                StartCoroutine(MoveGeneratorDown());
            }
        }
    }

    private void Update()
    {
        if (chargingAudioSource.isPlaying && timeToStopPlayingChargeAudio < Time.time)
        {
            chargingAudioSource.Stop();
        }
    }

    public void Done()
    {
        isEnabled = true;
        isFullyCharged = true;
        isDone = true;
        isReadyToSpin = false;
        Power = 0;
        spin.totalRotation = 0;
        capsuleCollider.enabled = false;
        gameObject.GetComponent<GeneratorFx>().SwitchToTickMat();
        OnActivateEvent.Invoke();
        if (GeneratorManager.Instance != null)
        {
            GeneratorManager.Instance.GeneratorsActivated++;
        }
        activatedAudioSource.PlayOneShot(activatedClip);
    }

    public void Enable()
    {
        isEnabled = true;
        isFullyCharged = false;
        isReadyToSpin = false;
        isDone = false;
        Power = 0;
        spin.totalRotation = 0;
        capsuleCollider.enabled = true;
        transform.localPosition = new Vector3(transform.localPosition.x,
            -5,
            transform.localPosition.z);
        StopAllCoroutines();
        StartCoroutine(MoveGeneratorUp());
    }
    
    public void Disable()
    {
        isEnabled = false;
        isFullyCharged = false;
        isDone = false;
        isReadyToSpin = false;
        Power = 0;
        spin.totalRotation = 0;
        capsuleCollider.enabled = false;
        gameObject.GetComponent<GeneratorFx>().SwitchToBlankMatDisabled();
        transform.localPosition = new Vector3(transform.localPosition.x,
            -4.9f,
            transform.localPosition.z);
    }

    private void Awake()
    {
        if (OnActivateEvent == null)
        {
            OnActivateEvent = new UnityEvent();
        }
    }

    // Use this for initialization
    private void Start()
    {
        spin = GetComponentInChildren<SpinDetection>();
        fx = GetComponent<GeneratorFx>();
        capsuleCollider = GetComponent<CapsuleCollider>();

        chargingAudioSource = GetComponents<AudioSource>()[0];
        chargingAudioSource.clip = chargingClip;
        chargingAudioSource.volume = 0.45f;
        
        activatedAudioSource = GetComponents<AudioSource>()[1];
        activatedAudioSource.volume = 0.75f;
        
        movingDownAudioSource = GetComponents<AudioSource>()[2];
        movingDownAudioSource.clip = movingDownClip;
        movingDownAudioSource.volume = 0.85f;
        
        Disable();
    }
    
    private void OnDestroy()
    {
        GeneratorCollection.RemoveGenerator(this);
    }
    
    public void IncreasePower()
    {
        if (isFullyCharged)
            return;

        Power += 1.5f * Time.deltaTime;
    }

    private IEnumerator MoveGeneratorDown()
    {
        float y = -5.4f;
        if (isEnabled)
        {
            gameObject.GetComponent<GeneratorFx>().SwitchToBlankMatSpinning();
        }
        else
        {
            gameObject.GetComponent<GeneratorFx>().SwitchToBlankMatDisabled();
        }
        
        movingDownAudioSource.Play();
        
        while(transform.localPosition.y > -4.9f)
        {
            yield return new WaitForEndOfFrame();
            transform.localPosition = new Vector3(transform.localPosition.x, Mathf.Lerp(transform.localPosition.y, y, Time.deltaTime*1.2f), transform.localPosition.z);
        }
        capsuleCollider.enabled = false;

        if (isEnabled)
        {
            gameObject.GetComponent<GeneratorFx>().SwitchToSpinMat();
        }
        movingDownAudioSource.Stop();
        isReadyToSpin = true;
    }

    private IEnumerator MoveGeneratorUp()
    {
        gameObject.GetComponent<GeneratorFx>().SwitchToElectricMat();
        while (transform.localPosition.y < -0.05f)
        {
            yield return new WaitForEndOfFrame();
            transform.localPosition = new Vector3(transform.localPosition.x, Mathf.Lerp(transform.localPosition.y, 0, Time.deltaTime), transform.localPosition.z);
        }
        isFullyCharged = false;
    }

    public void Hit()
    {
        IncreasePower();
    }
    
}
