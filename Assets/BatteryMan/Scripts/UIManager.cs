﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public float temporaryColorDuration = 0.45f;
    private Image powerSliderFill;
    
    private PlayerController player;
    private Slider powerSlider;
    private Color powerBarBaseColor;

    public Color PowerBarBaseColor
    {
        get { return powerBarBaseColor; }
        set
        {
            powerBarBaseColor = value;
            if (powerSliderFill.color != Color.red)
            {
                powerSliderFill.color = powerBarBaseColor;
            }
        }
    }

    public static UIManager Instance
    {
        get
        {
            if (_instance == null)
            {
                return null;
            }
            return _instance;
        }
    }

    private static UIManager _instance;

    void Awake()
    {
        _instance = this;
        player = GameObject.Find("Player").GetComponent<PlayerController>();
        powerSlider = GameObject.Find("PowerSlider").GetComponent<Slider>();
        powerSliderFill = GameObject.Find("PowerSliderFill").GetComponent<Image>();
        powerBarBaseColor = Color.yellow;
    }

    private void Start()
    {
        powerSlider.maxValue = player.maxPower;
    }

    // Update is called once per frame
    void Update()
    {
        powerSlider.value = player.Power;
    }

    public void SetTemporaryPowerBarColor(Color color)
    {
        StartCoroutine(TemporaryPowerBarColor(color));
    }

    private IEnumerator TemporaryPowerBarColor(Color color)
    {
        powerSliderFill.color = color;
        yield return new WaitForSeconds(temporaryColorDuration);
        powerSliderFill.color = powerBarBaseColor;
    }
    
}