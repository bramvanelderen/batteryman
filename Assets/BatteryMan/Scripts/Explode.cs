﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

//explodes when near player.
//also, chain explosions (if in explosion radius, this enemy should explode as well with slight delay)
public class Explode : MonoBehaviour, Interactable
{
    public GameObject Player;

    [SerializeField] [Range(0, 20)] float range = 3;

    [SerializeField] [Range(0, 200)] int damage = 50;

    private GameObject explosion;
    private bool exploded;
    private AudioSource audioSource;

    public int Damage
    {
        get { return damage; }

        set { damage = value; }
    }

    private void Awake()
    {
        explosion = GetComponentInChildren<GraphicExplosion>().gameObject;
        explosion.SetActive(false);
    }

    // Use this for initialization
    private void Start()
    {
        if (Player == null)
        {
            Player = GameObject.Find("Player");
        }
        exploded = false;
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    private void Update()
    {
        if (exploded) return;
        
        //Check if in range
        if (Vector3.Distance(Player.transform.position, gameObject.transform.position) < range)
        {
            Explosion();
        }
    }

    public void Explosion()
    {
        if (exploded) return;
        
        exploded = true;
        
        //Damage player
        if (Vector3.Distance(Player.transform.position, gameObject.transform.position) < range)
        {
            Player.GetComponent<PlayerController>().DecreasePowerBy(Damage);
        }

        //visual explosion effects and exploding nearby enemies (see PhysicalExplosion script)
        explosion.SetActive(true);
        
        //disabling the enemy model but keeping the explosion
        GetComponent<Rigidbody>().velocity = Vector3.zero;
        GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
        GetComponent<CapsuleCollider>().enabled = false;
        GetComponent<Animator>().enabled = false;
        GetComponent<Follow>().enabled = false;
        foreach (var trans in GetComponentsInChildren<Transform>())
        {
            if (!trans.CompareTag("Explosion") && !trans.CompareTag("Enemy"))
            {
                trans.gameObject.SetActive(false);
            }
        }

        //"no i will no charge your exploding phone!"
        if (Random.Range(0, 20) == 0)
        {
            audioSource.Play();
        }

        //destroy self
        Destroy(gameObject, 7f);
    }

    public void Hit()
    {
        Explosion();
    }
    
}