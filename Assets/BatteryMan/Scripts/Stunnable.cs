﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stunnable : MonoBehaviour, Interactable {

    public bool isStunned = false;
    public AudioClip stunnedClip;
    private AudioSource audioSource;
    private Follow follow;
    private ZapPlayer zapPlayer;
    private Health health;

    private void Start()
    {
        zapPlayer = GetComponent<ZapPlayer>();
        follow = GetComponent<Follow>();
        health = GetComponent<Health>();
        audioSource = GetComponent<AudioSource>();
    }

    public void Hit()
    {
//        if (isStunned)
//            return;

        StopAllCoroutines();
        StartCoroutine(Stun());

        print("STUNNED!");
        if (follow.isRanged)
        {
            follow.ResetAndHideRangeCircle();
        }
        
        health.RestoreAllHealth();
        if (!audioSource.isPlaying)
        {
            audioSource.PlayOneShot(stunnedClip);
        }
    }

    public IEnumerator Stun()
    {

        isStunned = true;
        
//        health.RestoreAllHealth();
//        if (!audioSource.isPlaying)
//        {
//            audioSource.PlayOneShot(stunnedClip);
//        }
//        zapPlayer.Damage = 0;
//        follow.Speed = 0;

        yield return new WaitForSeconds(3f);

//        zapPlayer.Damage = zapPlayer.originalDamage;
//        follow.Speed = follow.originalSpeed;

        isStunned = false;
    }
}
