﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance
    {
        get
        {
            if (_instance == null)
            {
                return null; //Dont initialise instance automaticly if its unsafe
            }
            return _instance;
        }
    }

    private static GameManager _instance;

    public int level = 1;

    private Color emissionColor;
    private PlayerController player;
    private GameObject playerModel;
    private GameObject cam;
    private GameObject UI;
    private GameObject titleUI;
    private GameObject gameOverUI;
    private GameObject spotlight;
    private GameObject worldlight;
    private Quaternion initialCamRotation;
    private Text levelText;

    void Awake()
    {
        _instance = this;
        levelText = GameObject.Find("LevelText").GetComponent<Text>();
        player = GameObject.Find("Player").GetComponent<PlayerController>();
        cam = GameObject.Find("Camera");
        UI = GameObject.Find("GameUI");
        gameOverUI = GameObject.Find("GOUI");
        titleUI = GameObject.Find("UI");
        foreach (var light in player.gameObject.GetComponentsInChildren<Light>())
        {
            if (light.name == "GOSpotlight")
            {
                spotlight = light.gameObject;
            }
        }
        worldlight = GameObject.Find("WorldLight");
        playerModel = GameObject.Find("batteryman");
        emissionColor = playerModel.GetComponentInChildren<Renderer>().material.GetColor("_EmissionColor");
        initialCamRotation = cam.transform.rotation;
    }

    private void Start()
    {
        //Must be active for find to work.
        gameOverUI.SetActive(false);

        level--;
        StartCoroutine(NextLevel());
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.LeftShift) && Input.GetKeyDown(KeyCode.P))
        {
            //refill power
            player.Power = player.maxPower;
        }
        else if (Input.GetKey(KeyCode.LeftShift))
        {
            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                GoToLevel(1);
            }
            else if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                GoToLevel(2);
            }
            else if (Input.GetKeyDown(KeyCode.Alpha3))
            {
                GoToLevel(3);
            }
            else if (Input.GetKeyDown(KeyCode.Alpha4))
            {
                GoToLevel(4);
            }
            else if (Input.GetKeyDown(KeyCode.Alpha5))
            {
                GoToLevel(5);
            }
            else if (Input.GetKeyDown(KeyCode.Alpha6))
            {
                GoToLevel(6);
            }
            else if (Input.GetKeyDown(KeyCode.Alpha7))
            {
                GoToLevel(7);
            }
            else if (Input.GetKeyDown(KeyCode.Alpha8))
            {
                GoToLevel(8);
            }
            else if (Input.GetKeyDown(KeyCode.Alpha9))
            {
                GoToLevel(9);
            }
            else if (Input.GetKeyDown(KeyCode.Alpha0))
            {
                GoToLevel(10);
            }
        }
    }

    public void GoToLevel(int levelNumber)
    {
        level = levelNumber - 1;
        StartCoroutine(NextLevel());
    }

    public IEnumerator NextLevel()
    {
        if (titleUI != null && level != 0)
        {
            titleUI.GetComponent<PlayMusic>().PlaySelectedMusic(3);
            yield return new WaitForSeconds(2f);
        }
        if (titleUI != null)
        {
            titleUI.GetComponent<PlayMusic>().PlayLevelMusic();
        }
        level++;
        levelText.text = "  LEVEL: " + level + "/10";

        SpawnManager.Instance.StopSpawning();
        //restore generators
        GeneratorManager.Instance.ResetGenerators();
        //reset player position
        player.transform.position = Vector3.zero + Vector3.up * 0.5f;
        //restore player power
        player.Power = player.maxPower;
        yield return new WaitForSeconds(2f);
        //start next level spawning
        SpawnManager.Instance.StartLevelSpawn();
    }

    public IEnumerator SmoothCameraLook()
    {
        var rotation = Quaternion.LookRotation(player.transform.position - cam.transform.position);
        while (cam.transform.rotation != rotation && player.GetComponent<PlayerController>().isGameOver)
        {
            cam.GetComponent<Camera>().fieldOfView =
                Mathf.Lerp(cam.GetComponent<Camera>().fieldOfView, 30, Time.deltaTime);
            cam.transform.rotation = Quaternion.Slerp(cam.transform.rotation, rotation, Time.deltaTime);
            yield return new WaitForEndOfFrame();
        }
    }

    public IEnumerator GameOver()
    {
        spotlight.GetComponent<Light>().enabled = true;

        Debug.Log(playerModel.GetComponentInChildren<Renderer>().material.name);
        playerModel.GetComponentInChildren<Renderer>().material.SetColor("_EmissionColor", Color.red);

        var n = cam.transform.position - playerModel.transform.position;
        playerModel.transform.rotation = Quaternion.LookRotation(n);
        playerModel.transform.eulerAngles = new Vector3(0, playerModel.transform.eulerAngles.y, 0);
        player.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
        player.GetComponent<Rigidbody>().velocity = Vector3.zero;
        UI.SetActive(false);
        gameOverUI.SetActive(true);
        worldlight.SetActive(false);
        StartCoroutine(FadeSpotlight());
        StartCoroutine(SmoothCameraLook());

        StopAllAudioClips();

        //Play the game over music
        if (titleUI != null)
        {
            titleUI.GetComponent<PlayMusic>().PlaySelectedMusic(2);
        }

        //Stop enemy spawning
        SpawnManager.Instance.StopSpawning();

        yield return null;
    }


    public IEnumerator FadeSpotlight()
    {
        Debug.Log("Called");
        while (spotlight.GetComponent<Light>().intensity > 0)
        {
            yield return new WaitForSeconds(0.2f);
            spotlight.GetComponent<Light>().intensity -= 0.3f;
        }
    }

    public IEnumerator Restart()
    {
        player.Power = player.maxPower;
        playerModel.GetComponentInChildren<Renderer>().material.SetColor("_EmissionColor", emissionColor);
        spotlight.transform.localEulerAngles = new Vector3(90, 0, 0);
        spotlight.transform.localPosition = new Vector3(0, 9, 0);

        worldlight.SetActive(true);
        playerModel.transform.localEulerAngles = new Vector3();

        player.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
        player.GetComponent<Rigidbody>().freezeRotation = true;
        
        //reset player position
        //todo later when we add levels that are split up, this position will be determined based on level
        player.transform.position = new Vector3(0, 0.5f, 0);
        
        UI.SetActive(true);
        gameOverUI.SetActive(false);
        spotlight.GetComponent<Light>().enabled = false;
        spotlight.GetComponent<Light>().intensity = 22;

        cam.GetComponent<Camera>().fieldOfView = 60;
        cam.transform.rotation = initialCamRotation;

        //Play the level music
        if (titleUI != null)
        {
            titleUI.GetComponent<PlayMusic>().PlayLevelMusic();
        }

        GeneratorManager.Instance.ResetGenerators();
        player.GetComponent<PlayerController>().isGameOver = false;
        
        SpawnManager.Instance.StopSpawning();
        SpawnManager.Instance.StartLevelSpawn();
        
        yield return null;
    }

    private void StopAllAudioClips()
    {
        foreach (AudioSource source in GameObject.FindObjectsOfType<AudioSource>())
        {
            source.Stop();
        }
    }

}