﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine.Events;

public class FloatEvent : UnityEvent<float> { }

public static class GlobalPower
{
    public static float PowerLevel {
        get
        {
            return _power;
        }
        set
        {
            _power = value;
            if (_power > 1)
                _power = 1;
            if (_power < 0)
                _power = 0;

            OnPowerChangeEvent.Invoke(_power);
        }
    }

    public static FloatEvent OnPowerChangeEvent
    {
        get
        {
            if (_onPowerChangeEvent == null)
                _onPowerChangeEvent = new FloatEvent();

            return _onPowerChangeEvent;
        }
    }

    private static float _power;
    private static FloatEvent _onPowerChangeEvent;
}
