﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    public static SpawnManager Instance
    {
        get
        {
            if (_instance == null)
            {
                return new GameObject("SpawnManager").AddComponent<SpawnManager>();
            }
            return _instance;
        }
    }

    private static SpawnManager _instance;

    public List<GameObject> spawnPoints;
    public GameObject basicEnemy;
    public GameObject bigEnemy;
    public GameObject rangedEnemy;
    public GameObject explodingEnemy;
    public bool onlySpawnOffScreen = false;

    [HideInInspector] public float spawnDelay = 2f;

    private int basicEnemiesToSpawn;
    private int bigEnemiesToSpawn;
    private int rangedEnemiesToSpawn;
    private int explodingEnemiesToSpawn;
    private GameObject enemiesFolder;

    private Camera cam;

    public void StopSpawning()
    {
        //stop any current spawning and remove enemies
        basicEnemiesToSpawn = bigEnemiesToSpawn = rangedEnemiesToSpawn = explodingEnemiesToSpawn = 0;
        if (enemiesFolder)
        {
            Destroy(enemiesFolder);
        }
        enemiesFolder = new GameObject("Enemies");
    }

    public void StartLevelSpawn()
    {
        StopSpawning();

        if (GameManager.Instance.level == 1)
        {
            basicEnemiesToSpawn = 4;
            bigEnemiesToSpawn = 0;
            rangedEnemiesToSpawn = 0;
            explodingEnemiesToSpawn = 0;
            spawnDelay = 2f;
        }
        else if (GameManager.Instance.level == 2)
        {
            basicEnemiesToSpawn = 2;
            bigEnemiesToSpawn = 4;
            rangedEnemiesToSpawn = 0;
            explodingEnemiesToSpawn = 0;
            spawnDelay = 1.9f;
        }
        else if (GameManager.Instance.level == 3)
        {
            basicEnemiesToSpawn = 2;
            bigEnemiesToSpawn = 2;
            rangedEnemiesToSpawn = 4;
            explodingEnemiesToSpawn = 0;
            spawnDelay = 1.8f;
        }
        else if (GameManager.Instance.level == 4)
        {
            basicEnemiesToSpawn = 4;
            bigEnemiesToSpawn = 0;
            rangedEnemiesToSpawn = 0;
            explodingEnemiesToSpawn = 6;
            spawnDelay = 1.7f;
        }
        else if (GameManager.Instance.level == 5)
        {
            basicEnemiesToSpawn = 2;
            bigEnemiesToSpawn = 2;
            rangedEnemiesToSpawn = 2;
            explodingEnemiesToSpawn = 2;
            spawnDelay = 1.6f;
        }
        else if (GameManager.Instance.level == 6)
        {
            basicEnemiesToSpawn = 3;
            bigEnemiesToSpawn = 3;
            rangedEnemiesToSpawn = 3;
            explodingEnemiesToSpawn = 3;
            spawnDelay = 1.5f;
        }
        else if (GameManager.Instance.level == 7)
        {
            basicEnemiesToSpawn = 4;
            bigEnemiesToSpawn = 3;
            rangedEnemiesToSpawn = 3;
            explodingEnemiesToSpawn = 4;
            spawnDelay = 1.4f;
        }
        else if (GameManager.Instance.level == 8)
        {
            basicEnemiesToSpawn = 8;
            bigEnemiesToSpawn = 4;
            rangedEnemiesToSpawn = 4;
            explodingEnemiesToSpawn = 4;
            spawnDelay = 1.3f;
        }
        else if (GameManager.Instance.level == 9)
        {
            basicEnemiesToSpawn = 10;
            bigEnemiesToSpawn = 5;
            rangedEnemiesToSpawn = 5;
            explodingEnemiesToSpawn = 5;
            spawnDelay = 1.2f;
        }
        else if (GameManager.Instance.level >= 10)
        {
            basicEnemiesToSpawn = 12;
            bigEnemiesToSpawn = 6;
            rangedEnemiesToSpawn = 6;
            explodingEnemiesToSpawn = 6;
            spawnDelay = 1.1f;
        }

        StartCoroutine(Spawn());
    }

    private void Awake()
    {
        _instance = this;
        cam = GameObject.Find("Camera").GetComponent<Camera>();
        enemiesFolder = new GameObject("Enemies");
    }

    private void Start()
    {
        if (cam == null) cam = FindObjectOfType<Camera>();
    }

    private bool IsOnScreen(Vector3 position)
    {
        Vector3 viewportPos = cam.WorldToViewportPoint(position);
        return (viewportPos.x > 0 && viewportPos.x < 1
                && viewportPos.y > 0 && viewportPos.y < 1
                && viewportPos.z > 0);
    }

    private IEnumerator Spawn()
    {
        while (basicEnemiesToSpawn + bigEnemiesToSpawn + rangedEnemiesToSpawn + explodingEnemiesToSpawn > 0)
        {
            bool valid = false;
            GameObject objectToSpawn = null;
            switch (Random.Range(0, 4))
            {
                case 0:
                    if (basicEnemiesToSpawn > 0)
                    {
                        valid = true;
                        objectToSpawn = basicEnemy;
                        basicEnemiesToSpawn--;
                    }
                    break;
                case 1:
                    if (bigEnemiesToSpawn > 0)
                    {
                        valid = true;
                        objectToSpawn = bigEnemy;
                        bigEnemiesToSpawn--;
                    }
                    break;
                case 2:
                    if (rangedEnemiesToSpawn > 0)
                    {
                        valid = true;
                        objectToSpawn = rangedEnemy;
                        rangedEnemiesToSpawn--;
                    }
                    break;
                case 3:
                    if (explodingEnemiesToSpawn > 0)
                    {
                        valid = true;
                        objectToSpawn = explodingEnemy;
                        explodingEnemiesToSpawn--;
                    }
                    break;
            }
            if (!valid) continue;

            if (onlySpawnOffScreen)
            {
                Instantiate(objectToSpawn, RandomOffScreenSpawnPoint(), Quaternion.identity, enemiesFolder.transform);
            }
            else
            {
                Instantiate(objectToSpawn, RandomSpawnPoint(), Quaternion.identity, enemiesFolder.transform);
            }

            yield return new WaitForSeconds(spawnDelay);
        }
    }

    private Vector3 RandomSpawnPoint()
    {
        return spawnPoints[Random.Range(0, spawnPoints.Count - 1)].transform.position;
    }

    private Vector3 RandomOffScreenSpawnPoint()
    {
        Vector3 spawnPoint = spawnPoints[Random.Range(0, spawnPoints.Count - 1)].transform.position;
        while (IsOnScreen(spawnPoint))
        {
            spawnPoint = spawnPoints[Random.Range(0, spawnPoints.Count - 1)].transform.position;
        }

        return spawnPoint;
    }
}