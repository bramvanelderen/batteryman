﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour {

    public float maxHealth = 100;
    [HideInInspector] public float health;
    
    private PhoneFx fx;
    
    private void Start()
    {
        fx = GetComponent<PhoneFx>();
        health = maxHealth / 1.6f;
        if (fx != null)
        {
            fx.UpdateBatteryFx(health / maxHealth);
        }
    }

    private void Update()
    {
        if (fx != null)
        {
            fx.UpdateBatteryFx(health / maxHealth);
        }
    }

    public void FixedUpdate()
    {
        health -= 0.2f;
    }

    public float GetHealthPercent()
    {
        return health / maxHealth;
    }

    public void RestoreAllHealth()
    {
        health = maxHealth;
    }
}
