﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerController : MonoBehaviour
{
    public GameObject stunAOE;
    public float maxPower = 1000;

    public float Power
    {
        get { return power; }
        set
        {
            if (power > maxPower) power = maxPower;
            power = value;
        }
    }

    private float stunCooldown;

    public bool isGameOver = false;

    public bool invunerable = false;

    public UnityEvent OnOutOfPower;

    private float power;

    public void DecreasePowerBy(float amount)
    {
        if (invunerable)
            return;
        Power -= amount;
    }

    void Start()
    {
        Power = maxPower;
        if (OnOutOfPower == null)
            OnOutOfPower = new UnityEvent();
    }

    void Update()
    {
        if (power > maxPower) power = maxPower;

        else if (!isGameOver && Power <= 0)
        {
            isGameOver = true;
            StartCoroutine(GameObject.Find("Main").GetComponent<InGameManager>().GameOver());
//            OnOutOfPower.Invoke();
        }

        //else if (Input.GetButtonDown("Stun"))
        //{
        //    if (UIManager.Instance == null)
        //        return;

        //    Power -= 2000;
        //    StartCoroutine(UIManager.Instance.SetTemporaryPowerBarColor(Color.cyan));
        //    Debug.Log("Stunning...");
        //    StartCoroutine(Stun());
        //}
    }

//    IEnumerator Stun()
//    {
//        stunAOE.SetActive(true);
//        yield return new WaitForSeconds(0.2f);
//        stunAOE.SetActive(false);
//    }
}