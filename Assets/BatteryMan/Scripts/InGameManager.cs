﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

/// <summary>
/// The game manager for the new system
/// </summary>
public class InGameManager : MonoBehaviour
{
    public float RoundDelay = 2f;

    /// <summary>
    /// Retrieve Level class from LevelManager using this level id to access all kinds of data for UI
    /// LevelManager.GetLevel(levelId)
    /// </summary>
    [HideInInspector]
    public int LevelId;
    [HideInInspector]
    public int RoundId;
    [HideInInspector]
    public bool InProgress = false;

    /// <summary>
    /// Subscribe to any event using onFinishGameEvent.AddListener()
    /// </summary>
    public UnityEvent OnFinishGameEvent;
    
    public AudioClip levelMusic;
    private AudioSource levelMusicPlayer;
    
    // Use this for initialization
    void Awake()
    {
        if (OnFinishGameEvent == null)
            OnFinishGameEvent = new UnityEvent();

        GlobalPower.PowerLevel = 1;

        levelMusicPlayer = gameObject.AddComponent<AudioSource>();
        levelMusicPlayer.clip = levelMusic;
        levelMusicPlayer.loop = true;
        levelMusicPlayer.volume = 0.75f;
        levelMusicPlayer.Play();
        
        //for gameover
        player = GameObject.Find("Player").GetComponent<PlayerController>();
        cam = GameObject.Find("Camera");
        UI = GameObject.Find("GameUI");
        gameOverUI = GameObject.Find("GOUI");
        if (gameOverUI != null)
        {
            gameOverUI.SetActive(false);
        }
        titleUI = GameObject.Find("UI");
        foreach (var light in player.gameObject.GetComponentsInChildren<Light>())
        {
            if (light.name == "GOSpotlight")
            {
                spotlight = light.gameObject;
            }
        }
        playerModel = GameObject.Find("batteryman");
        emissionColor = playerModel.GetComponentInChildren<Renderer>().material.GetColor("_EmissionColor");
        if (cam != null)
        {
            initialCamRotation = cam.transform.rotation;
        }
    }

    private void Start()
    {
        LevelManager.OnRoundEnd.AddListener(OnFinish);
        StartGame();
    }
    // Update is called once per frame
    void Update()
    {
        //reset logic
        if (InProgress && Input.GetKeyDown(KeyCode.R))
        {
            
            levelMusicPlayer.Stop();
            levelMusicPlayer.Play();
            
            //undo gameover stuff
            worldLight1.SetActive(true);
            worldLight2.SetActive(true);
            player.Power = player.maxPower;
            player.isGameOver = false;
            playerModel.GetComponentInChildren<Renderer>().material.SetColor("_EmissionColor", emissionColor);
            spotlight.transform.localEulerAngles = new Vector3(90, 0, 0);
            spotlight.transform.localPosition = new Vector3(0, 9, 0);
            playerModel.transform.localEulerAngles = new Vector3();

            player.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
            player.GetComponent<Rigidbody>().freezeRotation = true;
        
            UI.SetActive(true);
            UI.GetComponent<InGameUIManager>().SetPowerBarNormal();
            gameOverUI.SetActive(false);
            spotlight.GetComponent<Light>().enabled = false;
            spotlight.GetComponent<Light>().intensity = 22;

            cam.GetComponent<Camera>().fieldOfView = 60;
            cam.transform.rotation = initialCamRotation;

            //Play the level music
//            if (titleUI != null)
//            {
//                titleUI.GetComponent<PlayMusic>().PlayLevelMusic();
//            }
            
            LevelManager.GetLevel(LevelId).OutOfPowerListener();
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene("TitleScreenDynamic");
        }
    }

    public void StartGame()
    {
        LevelId = 0;
        RoundId = 0;

        var level = LevelManager.GetLevel(LevelId);
        if (!level.StartLevel(RoundId))
            return;

        InProgress = true;

//        print("Game started");
    }

    void OnFinish(Level level)
    {
        RoundId++;

        if (RoundId > level.FinalRoundId)
        {
            if (LevelManager.GetLevel(LevelId + 1) == null)
            {
                FinishGame();
                return;
            }
            LevelManager.OnLevelEnd.Invoke(level);
            LevelId++;
            RoundId = 0;
        }

        StartCoroutine(StartLevel(level));
    }    


    IEnumerator StartLevel(Level level)
    {
        yield return new WaitForSeconds(0f);
        level = LevelManager.GetLevel(LevelId);
        level.StartLevel(RoundId);
    }

    void FinishGame()
    {
        //Game is finished
        //        OnFinishGameEvent.Invoke();
        //        print("Game is finished");
        
        Initiate.Fade("TitleScreenDynamic", Color.white, 2f);
    }

    public GameObject worldLight1;
    public GameObject worldLight2;
    
    private Color emissionColor;
    private PlayerController player;
    private GameObject playerModel;
    private GameObject cam;
    private GameObject UI;
    private GameObject titleUI;
    private GameObject gameOverUI;
    private GameObject spotlight;
    private Quaternion initialCamRotation;
    
    public IEnumerator GameOver()
    {
        levelMusicPlayer.Stop();
        
        spotlight.GetComponent<Light>().enabled = true;

//        Debug.Log(playerModel.GetComponentInChildren<Renderer>().material.name);
        playerModel.GetComponentInChildren<Renderer>().material.SetColor("_EmissionColor", Color.red);

        var n = cam.transform.position - playerModel.transform.position;
        playerModel.transform.rotation = Quaternion.LookRotation(n);
        playerModel.transform.eulerAngles = new Vector3(0, playerModel.transform.eulerAngles.y, 0);
        player.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
        player.GetComponent<Rigidbody>().velocity = Vector3.zero;
        UI.SetActive(false);
        worldLight1.SetActive(false);
        worldLight2.SetActive(false);
        StartCoroutine(FadeSpotlight());
        StartCoroutine(SmoothCameraLook());

        StopAllAudioClips();
        gameOverUI.SetActive(true);

//        //Play the game over music
//        if (titleUI != null)
//        {
//            titleUI.GetComponent<PlayMusic>().PlaySelectedMusic(2);
//        }

        //Stop enemy spawning
        SpawnManager.Instance.StopSpawning();

        yield return null;
    }

    public IEnumerator SmoothCameraLook()
    {
        var rotation = Quaternion.LookRotation(player.transform.position - cam.transform.position);
        while (cam.transform.rotation != rotation && player.GetComponent<PlayerController>().isGameOver)
        {
            cam.GetComponent<Camera>().fieldOfView =
                Mathf.Lerp(cam.GetComponent<Camera>().fieldOfView, 30, Time.deltaTime);
            cam.transform.rotation = Quaternion.Slerp(cam.transform.rotation, rotation, Time.deltaTime);
            yield return new WaitForEndOfFrame();
        }
    }

    public IEnumerator FadeSpotlight()
    {
//        Debug.Log("Called");
        while (spotlight.GetComponent<Light>().intensity > 0)
        {
            yield return new WaitForSeconds(0.2f);
            spotlight.GetComponent<Light>().intensity -= 0.3f;
        }
    }
    
    private void StopAllAudioClips()
    {
        foreach (AudioSource source in GameObject.FindObjectsOfType<AudioSource>())
        {
            source.Stop();
        }
    }
}
