﻿using UnityEngine;
using System.Collections;
using TMPro;
using UnityEngine.UI;

public class InGameUIManager : MonoBehaviour
{
    [SerializeField] private InGameManager _igm;

    [SerializeField] private GameObject _generatorUIElement;
    [SerializeField] private GameObject _playerBarUIElement;

    [SerializeField] private TextMeshProUGUI _generatorText;
    [SerializeField] private Image powerBarImage;

    [SerializeField] private PlayerController _player;
    
    public float temporaryColorDuration = 0.45f;
    public Color zappingPowerBarColor = new Color(0,201,254);
    public Color damagedPowerBarColor = new Color(254,39,39);
    public Color normalPowerBarColor = new Color(254,225,111);
    private Color powerBarBaseColor;
    
    //todo (in future versions) have delay before starting next round (have to make sure cant wander into new area prior to that)

    /// <summary>
    /// Round tracking
    /// </summary>
    private bool _inRound = false;
    private float _activatedGenerators = 0;
    private float _totalGenerators = 0;
    private bool tempColorActivated = false;

    // Use this for initialization
    void Start()
    {
        ///Add all the listeners
        _igm.OnFinishGameEvent.AddListener(OnFinishGame);
        LevelManager.OnRoundStart.AddListener(OnStart);
        LevelManager.OnRoundEnd.AddListener(OnEnd);
        LevelManager.OnRoundFailed.AddListener(OnEnd);

        _generatorUIElement.SetActive(false);
        _playerBarUIElement.SetActive(false);

        powerBarBaseColor = normalPowerBarColor;
    }

    // Update is called once per frame
    void Update()
    {
        if (_inRound)
        {
            powerBarImage.fillAmount = _player.Power / _player.maxPower;
        }
    }

    public void SetPowerBarDamaged()
    {
        StopAllCoroutines();
        if (gameObject.activeSelf)
        {
            StartCoroutine(TemporaryPowerBarColor(damagedPowerBarColor));
        }
    }
    public void SetPowerBarZapping()
    {
        powerBarBaseColor = zappingPowerBarColor;
        if (!tempColorActivated)
        {
            powerBarImage.color = powerBarBaseColor;
        }
    }

    public void SetPowerBarNormal()
    {
        powerBarBaseColor = normalPowerBarColor;
        if (!tempColorActivated)
        {
            powerBarImage.color = powerBarBaseColor;
        }
    }
    
    public void SetPowerBarNormalOverride()
    {
        StopAllCoroutines();
        powerBarImage.color = normalPowerBarColor;
    }

    private IEnumerator TemporaryPowerBarColor(Color color)
    {
        tempColorActivated = true;
        powerBarImage.color = color;
        yield return new WaitForSeconds(temporaryColorDuration);
        powerBarImage.color = powerBarBaseColor;
        tempColorActivated = false;
    }


    public void OnUpdateGenerator()
    {

    }

    public void OnLevelStart(Level level)
    {

    }

    public void OnStart(Level level)
    {       

        _totalGenerators = level.Round.GeneratorAmount;
        _activatedGenerators = 0;
        level.OnGeneratorActiveEvent.AddListener(OnGeneratorActivate);
        _inRound = true;

        ///Enable generator UI
        _generatorUIElement.SetActive(true);
        _generatorText.text = 0 + "/" + _totalGenerators; 

        ///Enable player bar
        _playerBarUIElement.SetActive(true);
        powerBarImage.fillAmount = 1f;
    }

    public void OnEnd(Level level)
    {
        level.OnGeneratorActiveEvent.RemoveListener(OnGeneratorActivate);

        _inRound = false;
        ///Disable generator UI
        _generatorUIElement.SetActive(false);
        _playerBarUIElement.SetActive(false);
    }

    void OnGeneratorActivate()
    {
        if (!_igm.InProgress)
            return;

        _activatedGenerators++;
        ///Update UI here
        _generatorText.text = _activatedGenerators + "/"  + _totalGenerators;
    }



    /// <summary>
    /// In game UI disabled after game is done
    /// </summary>
    public void OnFinishGame()
    {
    }
}
