﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpinDetection : MonoBehaviour
{
	
	public GameObject generatorTop;
	public float totalRotation;
	public float requiredRotation = 2000;
	public float powerToRestore = 120;

	private GeneratorController generator;
	private GeneratorFx fx;
	private PlayerController player;
	private AudioSource audioSource;

	private float startRotation;
	private bool onSpinCircle;

	private void Awake()
	{
		player = GameObject.Find("Player").GetComponent<PlayerController>();
	}

	void Start () {
		generator = GetComponentInParent<GeneratorController>();
		fx = GetComponentInParent<GeneratorFx>();
		audioSource = GetComponent<AudioSource>();
		totalRotation = 0;
	}

	private void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.CompareTag("Player") && !generator.isDone)
		{
			startRotation = player.transform.eulerAngles.y;
			onSpinCircle = true;
		}
	}

	private void OnTriggerExit(Collider other)
	{
		onSpinCircle = false;
	}

	private void Update()
	{
		if (onSpinCircle && generator.isReadyToSpin && !generator.isDone)
		{
			float newRotation = player.transform.eulerAngles.y;
			totalRotation += Math.Abs(startRotation - newRotation);
			player.Power += Math.Abs(startRotation - newRotation) / requiredRotation * powerToRestore;

			if (Math.Abs(startRotation - newRotation) > 8f)
			{
				if (!audioSource.isPlaying)
				{
					audioSource.Play();
				}
			}
			else
			{
				audioSource.Stop();
			}
			
			startRotation = newRotation;
			generatorTop.transform.eulerAngles = new Vector3(-89.98f, -totalRotation, 0);
			
			fx.T = Mathf.Clamp01(totalRotation / requiredRotation);

			if (totalRotation >= requiredRotation)
			{
				generatorTop.transform.eulerAngles = new Vector3(-89.98f, 0, 0);
				audioSource.Stop();
				generator.Done();
			}
			
		}
	}

}
