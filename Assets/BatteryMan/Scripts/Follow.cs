﻿using System.Collections;
using UnityEngine;

public class Follow : MonoBehaviour {

    public GameObject followTarget;
    public bool isRanged = false;
    public float rangeExpandRatePerSecond = 4f;
    [HideInInspector] public float originalSpeed;

    //Make these editable in the UnityEditor
    [SerializeField]
    [Range(0, 100)] private float speed;
    [SerializeField]
    [Range(0f, 20f)] private float closestDistance;

    [SerializeField]
    [Range(0f, 1f)] private float greenHealthThreshold = 0.5f, yellowHealthThreshold = 0.25f;
    
    [SerializeField]
    [Range(0f, 10f)] private float greenHealthMod = 0.2f, yellowHealthMod = 1f, redHealthMod = 2f;

    private Rigidbody rb;
    private Animator anim;
    private bool expanding = false;

    public float Speed
    {
        get
        {
            return speed;
        }

        set
        {
            speed = value;
        }
    }

    private Vector3 randomOffset;
    private bool isStopped = false;
    private GameObject rangeCircle;
    private ZapPlayer zp;
    private Stunnable stunnable;

    private void Awake()
    {
        randomOffset = new Vector3(0, 0, Random.Range(-2f,2f));

        originalSpeed = speed;
        
        foreach (var mesh in GetComponentsInChildren<MeshRenderer>())
        {
            if (mesh.gameObject.name == "RangeCircle")
            {
                rangeCircle = mesh.gameObject;
//                print("RANGE CIRCLE FOUND");
                break;
            }
        }
    }

    private void Start () {
        //If missing, attach
        if(followTarget == null)
        {
            followTarget = GameObject.Find("Player");
        }
        //Easier handling of rigidbody.
        rb = gameObject.GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
        zp = GetComponent<ZapPlayer>();
        stunnable = GetComponent<Stunnable>();
	}

    private void Update () {
        
        //Stop moving if game over
        if (followTarget.GetComponent<PlayerController>().isGameOver)
        {
            if (!isStopped)
            {
                isStopped = true;
                StopMovement();
            }
            return;
        }

        //Shorter names
        Vector3 here = transform.position,
                there = followTarget.transform.position;

	    //add some randomization to there
	    there = there + randomOffset;
	    
        //Get the health's speed modifier
        float healthmod = 1;
        float healthpercent = gameObject.GetComponent<Health>().GetHealthPercent();
        if (healthpercent >= greenHealthThreshold) {
            healthmod = greenHealthMod;
            StopMovement();
        }
        else if (healthpercent >= yellowHealthThreshold) {
            healthmod = yellowHealthMod;
        }
        else {
            healthmod = redHealthMod;
        }
        
        //Continuously look at target if has some speed
        if (speed > 0)
        {
            transform.LookAt(there);
        }

        if (isRanged && expanding)
        {
            if (Vector3.Distance(there, here) > closestDistance + zp.originalRange)
            {
                ResetAndHideRangeCircle();
                isStopped = false;
                //Calculate velocity to move the gameobject
                rb.velocity = Vector3.Normalize(there - here) * 0.1f * Speed * healthmod;
                anim.SetFloat("Speed", speed);
            }
        }
        //follow if far away
        else if (healthpercent < greenHealthThreshold && Vector3.Distance(there, here) > closestDistance)
        {
            isStopped = false;
            //Calculate velocity to move the gameobject
            rb.velocity = Vector3.Normalize(there - here) * 0.1f * Speed * healthmod;
            anim.SetFloat("Speed", speed);
        }
        //Stop moving if too close
        else
        {
            StopMovement();
            if (isRanged && !expanding && (stunnable == null || !stunnable.isStunned))
            {
                //expand attack circle
                expanding = true;
                StartCoroutine(ExpandRangeCircle());
            }
        }

        if (stunnable != null && stunnable.isStunned)
        {
            if (rangeCircle != null)
            {
                StopAllCoroutines();
                ResetAndHideRangeCircle();
            }
        }
        
    }

    public void ResetAndHideRangeCircle()
    {
        StopAllCoroutines();
        expanding = false;
        zp.range = zp.originalRange;
        rangeCircle.SetActive(false);
        rangeCircle.transform.localScale = new Vector3(zp.originalRange*2,1,zp.originalRange*2);
    }

    public void ShowRangeCircle()
    {
        expanding = true;
        rangeCircle.SetActive(true);
    }

    private IEnumerator ExpandRangeCircle()
    {   
        yield return new WaitForSeconds(0.31f);
        
        ShowRangeCircle();
        
        while (zp.range < closestDistance + zp.originalRange)
        {
            zp.range += rangeExpandRatePerSecond / 100f;
            if (rangeCircle)
            {
                rangeCircle.transform.localScale = new Vector3(zp.range*2,1,zp.range*2);
            }
            yield return new WaitForSeconds(0.01f);
        }
        //wait
//        yield return new WaitForSeconds(2f);
        
//        ResetAndHideRangeCircle();
        
        //then undo expansion
//        while (zp.range > zp.originalRange)
//        {
//            zp.range -= rangeExpandRatePerSecond / 100f;
//            if (rangeCircle)
//            {
//                rangeCircle.transform.localScale = new Vector3(zp.range*2,1,zp.range*2);
//            }
//            yield return new WaitForSeconds(0.01f);
//        }
        
    }

    private void StopMovement()
    {
        rb.velocity = Vector3.zero;
        anim.SetFloat("Speed", 0);
    }
}
