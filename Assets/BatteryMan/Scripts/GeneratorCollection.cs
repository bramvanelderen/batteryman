﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;


/// <summary>
/// TODO MAYBE REDUNDAND PROBABLY REMOVE
/// </summary>
public class GeneratorCollection
{
    private static GeneratorCollection _instance;
    public static GeneratorCollection Instance
    {
        get
        {
            if (_instance == null)
                _instance = new GeneratorCollection();

            return _instance;
        }
    }

    private List<GeneratorController> _generators;

    public GeneratorCollection()
    {
        _generators = new List<GeneratorController>();
    }


    public static List<GeneratorController> GetGenerators(int levelId)
    {
        var instance = Instance;

        return null;
        //return instance._generators.FindAll(x => x.LevelId == levelId);
    }

    public static void AddGenerator(GeneratorController gen)
    {
        var instance = Instance;

        instance._generators.Add(gen);

    }

    public static void RemoveGenerator(GeneratorController gen)
    {
        var instance = Instance;

        if (instance._generators.Any(x => x == gen))
        {
            instance._generators.Remove(gen);
        }
    }

}
