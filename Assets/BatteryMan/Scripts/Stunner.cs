﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stunner : MonoBehaviour {

    private void OnTriggerEnter(Collider other)
    {
        print("other tag: "+other.tag);
        if (other.GetComponent<Stunnable>()!=null && false == other.GetComponent<Stunnable>().isStunned)
        {
            Debug.Log("Stunned!");
            other.GetComponent<Stunnable>().isStunned = true;
            other.GetComponent<Stunnable>().StartCoroutine(other.GetComponent<Stunnable>().Stun());
        }
        else if (other.GetComponent<Explode>()!=null)
        {
            Debug.Log("Exploded enemy using electricity!");
            other.GetComponent<Explode>().Explosion();
        }
    }

}
