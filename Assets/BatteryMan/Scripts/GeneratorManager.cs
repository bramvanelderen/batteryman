﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GeneratorManager : MonoBehaviour
{
    public static GeneratorManager Instance
    {
        get
        {
            if (_instance == null)
            {
                return null;
            }
            return _instance;
        }
    }

    private static GeneratorManager _instance;

    public int maxGenerators = 6;

    private int generatorsActivated;
    
    public int GeneratorsActivated
    {
        get { return generatorsActivated; }
        set
        {
            generatorsActivated = value;
            worldLight.intensity = generatorsActivated / (float)maxGenerators * 0.8f;
            UpdateGeneratorsText();
            if (generatorsActivated == maxGenerators)
            {
                StartCoroutine(GameManager.Instance.NextLevel());
            }
        }
    }

    private Text generatorsText;
    private GameObject[] generators;
    private Light worldLight;
    

    private void UpdateGeneratorsText()
    {        
        generatorsText.text = "GENERATORS CHARGED: " + generatorsActivated + "/" + maxGenerators + "  ";
    }

    // Use this for initialization
    private void Awake()
    {
        _instance = this;
        generatorsActivated = 0;
        generatorsText = GameObject.Find("GeneratorsText").GetComponent<Text>();
        UpdateGeneratorsText();
        generators = GameObject.FindGameObjectsWithTag("Generator");
        worldLight = GameObject.Find("WorldLight").GetComponent<Light>();
        worldLight.intensity = 0.05f;
    }

    public void ResetGenerators()
    {
        GeneratorsActivated = 0;
        foreach (var generator in generators)
        {
            generator.GetComponent<GeneratorController>().Enable();
        }
    }
}