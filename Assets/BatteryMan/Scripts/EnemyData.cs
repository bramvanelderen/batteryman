﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[CreateAssetMenu(fileName = "EnemyData", menuName = "Data/EnemyData")]
public class EnemyData : ScriptableObject
{
    private static EnemyData _instance;
    public static EnemyData Instance
    {
        get
        {
            if (_instance == null)
                _instance = Resources.Load<EnemyData>("EnemyData");

            return _instance;
        }
    }

    public static GameObject GetEnemyPrefab(EnemyType type)
    {
        var instance = Instance;

        if (!instance._enemies.Any(x => x.Type == type))
            return null;

        return instance._enemies.Find(x => x.Type == type).Prefab;
    }

    [SerializeField]
    List<Enemy> _enemies;
}

[System.Serializable]
public struct Enemy
{
    public EnemyType Type;
    public GameObject Prefab;
}