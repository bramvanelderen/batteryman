﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZapPlayer : MonoBehaviour {

    public GameObject Player;
    public AudioClip attackClip;

    [Range(0, 20)]
    public float originalRange;
    [HideInInspector] public float range;

    [SerializeField]
    [Range(0, 20)]
    float zapCooldownInSeconds = 1.5f;
    private float timeOfLastZap = 0f;

    [SerializeField]
    [Range(0, 200)]
    int damage = 5;

    [HideInInspector] public int originalDamage;

    [SerializeField]
    [Range(0f, 1f)]
    float greenHealthThreshold = 0.5f, yellowHealthThreshold = 0.25f;

    [SerializeField]
    [Range(0f, 10f)]
    float greenHealthMod = 0.5f, yellowHealthMod = 1f, redHealthMod = 2f;

    [SerializeField]
    ParticleSystem _ps;

    private Health health;
    //todo make managers singletons and get rid of 'Main' gameobject
    private InGameUIManager gameUI;

    public int Damage
    {
        get
        {
            return damage;
        }

        set
        {
            damage = value;
        }
    }

    // Use this for initialization
    void Start () {

        if (Player == null)
        {
            Player = GameObject.Find("Player");
        }
        health = GetComponent<Health>();
        range = originalRange;
        timeOfLastZap = 0;
        originalDamage = damage; // this is counterintuitive to the naming for range and originalRange, but deal with it lol
        gameUI = GameObject.Find("GameUI").GetComponent<InGameUIManager>();
    }
	
	// Update is called once per frame
	void Update () {
	    //first check if health isn't green
	    if (health.health < 0.5f)
	    {
	        //then check if in range and cooldown done
	        if((Vector3.Distance(Player.transform.position, gameObject.transform.position) < range)
	           && (Time.time > (timeOfLastZap+zapCooldownInSeconds)) )
	        {
	            //Check for line of sight to the player.
	            RaycastHit hit;
	            Vector3 direction = Player.transform.position - transform.position;
	            if(GetComponent<Animator>().runtimeAnimatorController.name == "PhoneAddictRanged"
	               || (Physics.Raycast(transform.position, direction, out hit, range) && hit.collider.CompareTag("Player"))){
	                Zap();
	            }
	        }
	    }
	}

    void Zap()
    {
        float damageMod = 1f;
        //Adjust damage value
        float healthpercent = health.GetHealthPercent();
        if (healthpercent > greenHealthThreshold)
        {
            damageMod = greenHealthMod;
        }
        else if (healthpercent > yellowHealthThreshold)
        {
            damageMod = yellowHealthMod;
        }
        else
        {
            damageMod = redHealthMod;
        }

        //Start the cooldown
        timeOfLastZap = Time.time;

        //Damage player
        Player.GetComponent<PlayerController>().DecreasePowerBy(Damage * damageMod);
        
        //Refill some enemy battery power
        gameObject.GetComponent<Health>().health += gameObject.GetComponent<Health>().maxHealth / 2.4f;

        //Play audio if caused damage
        if (Damage*damageMod > 0)
        {
            GetComponent<AudioSource>().Play();
        }

        if (_ps != null)
        {
            _ps.Emit(100);
        }

        if (gameUI.isActiveAndEnabled)
        {
            gameUI.SetPowerBarDamaged();
        }
    }
}
