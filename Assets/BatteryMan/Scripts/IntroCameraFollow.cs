﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntroCameraFollow : MonoBehaviour
{

	private Transform player;
	private Vector3 offset;
	[SerializeField]
	private float sideOffset = 30;

	void Awake ()
	{
		player = GameObject.Find("Player").transform;
		offset = transform.position - player.position;
	}

	// Update is called once per frame
	void LateUpdate ()
	{
		
		Vector3 cameraPos = new Vector3((player.position + offset).x, transform.position.y, player.position.z - 5);
		if (cameraPos.x >= sideOffset)
		{
			cameraPos.x = sideOffset;
		}
		else if (cameraPos.x <= -sideOffset)
		{
			cameraPos.x = -sideOffset;
		}
		transform.position = cameraPos;
	}
}
