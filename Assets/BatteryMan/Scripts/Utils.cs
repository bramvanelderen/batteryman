﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Audio;

/// <summary>
/// An audio wrapper which makes it easy to play clips with a variety of custom settings
/// </summary>
[System.Serializable]
public class CustomClip
{
    [Header("Clip Settings")]
    public List<AudioClip> Clips = new List<AudioClip> { null };
    public float Volume = 1f;
    public Range Pitch = new Range() { Min = 1f, Max = 1f };
    public Range LowPass = new Range() { Min = 1f, Max = 1f };
    public float Reverb = 1f;
    public bool Loop = false;
    public AudioMixerGroup Group;

    [Header("Spacial Blend Settings")]
    public float SpacialBlend = 0f;
    public AudioRolloffMode Rolloff = AudioRolloffMode.Linear;
    public Range Distance = new Range() { Min = 1f, Max = 50f };

    AudioClip Clip
    {
        get
        {
            if (Clips == null || Clips.Count == 0)
                return null;

            return Clips[UnityEngine.Random.Range(0, Clips.Count)];
        }
    }

    /// <summary>
    /// Play an audio clip with all the custom settings
    /// </summary>
    /// <param name="audio"></param>
    public void Play(AudioSource audio)
    {
        if (Clip == null)
            return;

        //Apply filters
        var lowPass = audio.gameObject.GetComponent<AudioLowPassFilter>();
        if (!lowPass)
            lowPass = audio.gameObject.AddComponent<AudioLowPassFilter>();
        lowPass.cutoffFrequency = 22000f * LowPass.Random;

        //Apply Audio source settings and play
        audio.Stop();
        audio.clip = Clip;
        audio.volume = Volume;
        audio.spatialBlend = SpacialBlend;
        audio.pitch = Pitch.Random;
        audio.loop = Loop;
        audio.reverbZoneMix = Reverb;
        audio.rolloffMode = Rolloff;
        audio.minDistance = Distance.Min;
        audio.maxDistance = Distance.Max;
        audio.outputAudioMixerGroup = Group;
        audio.Play();
    }

    public void Play(MonoBehaviour component)
    {
        var audio = component.GetComponent<AudioSource>();
        if (!audio)
            audio = component.gameObject.AddComponent<AudioSource>();

        Play(audio);
    }

    public static AudioSource CreateAudioSource()
    {
        var obj = new GameObject();
        obj.name = "TempAudioSource";
        var audio = obj.AddComponent<AudioSource>();
        audio.playOnAwake = false;

        return audio;
    }

    public static AudioSource CreateAudioSource(Component comp)
    {
        var obj = new GameObject();
        obj.transform.parent = comp.transform;
        obj.name = "TempAudioSource";
        var audio = obj.AddComponent<AudioSource>();
        audio.playOnAwake = false;

        return audio;
    }
}

/// <summary>
/// Range class
/// </summary>
[System.Serializable]
public class Range
{
    public float Min;
    public float Max;

    /// <summary>
    /// Returns a random value within the min max range
    /// </summary>
    public float Random
    {
        get
        {
            return UnityEngine.Random.Range(Min, Max);
        }
    }
}