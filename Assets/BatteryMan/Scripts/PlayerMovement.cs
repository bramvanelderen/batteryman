﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


[RequireComponent(typeof(Rigidbody))]
public class PlayerMovement: MonoBehaviour
{
    [SerializeField]
    private float _speed = 10f;
    [SerializeField]
    private float _angularSpeed = 8f;
    [SerializeField]
    private float _stickDeadzone = .2f;
    [SerializeField]
    private float _mouseDeadzone = .05f;
    [SerializeField, Tooltip("0 is instant, higher values is delayed")]
    private float _fallBackDuration = 0f;

    private Rigidbody _rb;
    private Vector3 _inputLs = Vector3.zero;
    private Vector3 _inputRs = Vector3.zero;
    private Vector3 _mousePos = Vector3.zero;
    private Vector3 _velocity = Vector3.zero;
    private InputType _lastInput = InputType.Kbm;
    private Vector3 _inputDirection = Vector3.zero;

    private Animator _anim;

    public Vector3 Direction { get
        {
            return _inputDirection;
        }
    }

    private void Start()
    {
        _rb = GetComponent<Rigidbody>();
        _anim = GetComponent<Animator>();
    }

    private void Update()
    {
        //Disable movement input on gameover
        if (gameObject.GetComponent<PlayerController>().isGameOver)
            return;

        _inputLs.x = Input.GetAxis("Horizontal");
        _inputLs.z = Input.GetAxis("Vertical");
        Vector3.ClampMagnitude(_inputLs, 1f);

        _inputRs.x = Input.GetAxis("HorizontalR");
        _inputRs.z = Input.GetAxis("VerticalR");
        Vector3.ClampMagnitude(_inputRs, 1f);

        if (_inputRs.magnitude > _stickDeadzone)
            _lastInput = InputType.Gamepad;
        if (Vector3.Distance(_mousePos, Input.mousePosition) > _mouseDeadzone)
            _lastInput = InputType.Kbm;

        switch (_lastInput)
        {
            case InputType.Kbm:
                _mousePos = Vector3.Scale(Input.mousePosition, new Vector3(1, 1, 0));
                var playerPos = Vector3.Scale(Camera.main.WorldToScreenPoint(transform.position),
                    new Vector3(1, 1, 0));
                var dir = (_mousePos - playerPos);
                dir.z = dir.y;
                dir.y = 0;
                _inputDirection = dir.normalized;
                break;
            case InputType.Gamepad:
                if (_inputRs.magnitude > _stickDeadzone)
                    _inputDirection = _inputRs;
                break;
        }

        Move();
        Turn();

        
        var rotation = Quaternion.Euler(0, ((Mathf.Atan2((_inputDirection.x * -1), _inputDirection.z)) * Mathf.Rad2Deg), 0);
        var animVector = rotation * _inputLs;
        _anim.SetFloat("Direction", animVector.x);
        _anim.SetFloat("Speed", animVector.z);
    }

    void Move()
    {
        if (_inputLs.magnitude < _stickDeadzone)
        {
            var vel = Vector3.Scale(_rb.velocity, new Vector3(1, 0, 1));
            vel = Vector3.SmoothDamp(vel, Vector3.zero, ref _velocity, _fallBackDuration);
            _rb.velocity = Vector3.Scale(_rb.velocity, Vector3.up) + vel;
        } else
        {
            _rb.velocity = _inputLs * _speed;
        }
    }

    void Turn()
    {
        var rbEuler = _rb.rotation.eulerAngles;
        var euler = new Vector3(rbEuler.x, (Mathf.Atan2(_inputDirection.x, _inputDirection.z) * Mathf.Rad2Deg), rbEuler.z);
        _rb.MoveRotation(Quaternion.Lerp(_rb.rotation, Quaternion.Euler(euler), _angularSpeed * Time.deltaTime));  
    }


    private void FixedUpdate()
    {
        //Move();
        //Turn();
    }

    enum InputType
    {
        Kbm,
        Gamepad
    }
}

