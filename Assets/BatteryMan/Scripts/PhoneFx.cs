﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhoneFx : MonoBehaviour {

    [SerializeField] private Color lowBatteryColor = Color.red;
    [SerializeField] private Color mediumBatteryColor = Color.yellow;
    [SerializeField] private Color highBatteryColor = Color.green;
    public float emissionMin = 2;
    public float emissionMax = 5;

    private SkinnedMeshRenderer phoneMesh;
    
    [SerializeField]
    [Range(0f, 1f)]
    float greenHealthThreshold = 0.5f, yellowHealthThreshold = 0.25f;

    private Color colorMin;
    private Color colorMax;
    private float t;

    private void Awake()
    {
        phoneMesh = GetComponentInChildren<SkinnedMeshRenderer>();
    }

    public float T {
        get
        {
            return t;
        }
        set
        {
            t = value;
            var emission = Mathf.Lerp(emissionMin, emissionMax, t);
            
            Color color = Color.Lerp(colorMin, colorMax, t);
            
            phoneMesh.material.SetColor("_EmissionColor", color * emission);

        }
    }

    public void UpdateBatteryFx(float percent)
    {
        if (percent >= greenHealthThreshold)
        {
            colorMin = highBatteryColor;
            colorMax = highBatteryColor;
            T = 1;
        }
        else if (percent < yellowHealthThreshold)
        {
            colorMin = lowBatteryColor;
            colorMax = lowBatteryColor;
            T = 1; 
        }
        else {
            colorMin = mediumBatteryColor;
            colorMax = lowBatteryColor;
            T = 1 - percent / greenHealthThreshold;
        }
        
//        else if (percent > yellowHealthThreshold)
//        {
//            colorMin = Color.yellow;
//            colorMax = Color.green;
//            T = (percent - yellowHealthThreshold) / greenHealthThreshold;
//        }
//        else
//        {
//            colorMin = Color.red;
//            colorMax = Color.yellow;
//            T = percent / yellowHealthThreshold;
//        }
    }
  
}
