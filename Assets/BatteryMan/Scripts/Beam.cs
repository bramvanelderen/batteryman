﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PlayerController), typeof(PlayerMovement))]
public class Beam : MonoBehaviour
{
    [SerializeField] private float beamRange = 10f;
    [SerializeField] private ParticleSystem onHitParticles;
    [SerializeField] private List<ParticleSystem> beamParticles;
    [SerializeField] private Transform particlePlane;
    [SerializeField] private CustomClip _beamAudio;

    public float beamDrain = 30f;

    private Animator _anim;
    private PlayerController _pc;
    private PlayerMovement _pm;
    private AudioSource _audio;

    private bool _isFiring = false;
    private InGameUIManager gameUI;

    // Use this for initialization
    private void Start()
    {
        _pc = GetComponent<PlayerController>();
        _pm = GetComponent<PlayerMovement>();
        _audio = CustomClip.CreateAudioSource(this);
        _anim = GetComponent<Animator>();
        gameUI = GameObject.Find("GameUI").GetComponent<InGameUIManager>();
        SetBeam(false);
    }

    // Update is called once per frame
    private void Update()
    {
        if (_pc.isGameOver)
        {
            _audio.Stop();
            SetBeam(false);
            return;
        }

        if (!_isFiring && Input.GetButtonDown("Fire1"))
        {
            SetBeam(true);
        }
        else if (_isFiring && Input.GetButtonUp("Fire1"))
        {
            SetBeam(false);
        }

        FireBeam();
    }

    void FireBeam()
    {
        if (!_isFiring)
        {
            gameUI.SetPowerBarNormal();
            return;
        }
        
        gameUI.SetPowerBarZapping();

        var laserEndPosition = particlePlane.localPosition;
        var direction = _pm.Direction;
        RaycastHit hit;

        if (onHitParticles.isPlaying)
                onHitParticles.Stop();
        laserEndPosition.z = beamRange;
        
        if (
            Physics.BoxCast(
                transform.position, 
                new Vector3(.3f, .2f, .2f), direction, 
                out hit, 
                transform.rotation, 
                beamRange))
        {
            ///Hit wall or enemy
            var interactiveComp = hit.collider.GetComponent<Interactable>();
            if (interactiveComp != null)
                interactiveComp.Hit();

            if (!onHitParticles.isPlaying)
                onHitParticles.Play();

            onHitParticles.gameObject.transform.position = hit.point + Vector3.up;
            onHitParticles.transform.forward = direction * -1;
            laserEndPosition.z = Vector3.Distance(transform.position, hit.point);
        }

        particlePlane.localPosition = laserEndPosition;
        _pc.DecreasePowerBy(beamDrain * Time.deltaTime);
    }
    

    void SetBeam(bool enabled)
    {
        //lr.enabled = enabled;
        _anim.SetBool("IsFiring", enabled);
        _isFiring = enabled;
        
        if (enabled)
        {
            if (UIManager.Instance != null)
                UIManager.Instance.PowerBarBaseColor = Color.cyan;

            _beamAudio.Play(_audio);
            foreach(var system in beamParticles)
            {
                system.Play();
            }
        } else
        {
            if (UIManager.Instance != null)
                UIManager.Instance.PowerBarBaseColor = Color.yellow;

            _audio.Stop();
            foreach (var system in beamParticles)
            {
                system.Stop();
                system.SetParticles(null, 0);
            }
            onHitParticles.Stop();
        }
    }
}
