﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Events;

public class LevelEvent : UnityEvent<Level> { }

public static class LevelManager
{

    private static List<Level> _levels;
    private static List<Level> Levels
    {
        get
        {
            if (_levels == null)
                _levels = new List<Level>();

            return _levels;
        }
    }



    public static LevelEvent OnRoundPrepare {
        get
        {
            if (_OnRoundPrepare == null)
                _OnRoundPrepare = new LevelEvent();
            return _OnRoundPrepare;
        }
    }
    public static LevelEvent OnRoundStart
    {
        get
        {
            if (_OnRoundStart == null)
                _OnRoundStart = new LevelEvent();
            return _OnRoundStart;
        }
    }
    public static LevelEvent OnRoundFailed
    {
        get
        {
            if (_OnRoundFailed == null)
                _OnRoundFailed = new LevelEvent();
            return _OnRoundFailed;
        }
    }
    public static LevelEvent OnRoundEnd
    {
        get
        {
            if (_OnRoundEnd == null)
                _OnRoundEnd = new LevelEvent();
            return _OnRoundEnd;
        }
    }
    public static LevelEvent OnLevelStart
    {
        get
        {
            if (_OnLevelStart == null)
                _OnLevelStart = new LevelEvent();
            return _OnLevelStart;
        }
    }
    public static LevelEvent OnLevelEnd
    {
        get
        {
            if (_OnLevelEnd == null)
                _OnLevelEnd = new LevelEvent();
            return _OnLevelEnd;
        }
    }

    private static LevelEvent _OnRoundPrepare;
    private static LevelEvent _OnRoundStart;
    private static LevelEvent _OnRoundFailed;
    private static LevelEvent _OnRoundEnd;
    private static LevelEvent _OnLevelStart;
    private static LevelEvent _OnLevelEnd;


    public static Level GetLevel(int levelId)
    {
        var level = Levels.Find(x => x.Index == levelId);
        if (level == null)
            return null;

        return level;
    }    

    public static bool CanStartLevel()
    {
        foreach(var level in Levels)
        {
            if (level.IsInProgress)
                return false;
        }

        return true;
    }

    public static void AddLevel(Level level)
    {

        if (!Levels.Any(x => x == level))
        {
            Levels.Add(level);
        }

    }

    public static void RemoveLevel(Level level)
    {
        if (Levels.Any(x => x == level))
        {
            Levels.Remove(level);
        }
    }
}

