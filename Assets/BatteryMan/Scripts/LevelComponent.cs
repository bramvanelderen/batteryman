﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public abstract class LevelComponent : MonoBehaviour
{
    public abstract void OnRoundPrepare();
    public abstract void OnRoundBegin();
    public abstract void OnLevelEnd();

}

