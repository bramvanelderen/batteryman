﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

public class OnTriggerEvent : UnityEvent<GameObject> { }

public class OnTriggerEventHandler : MonoBehaviour
{
    [SerializeField]
    private string _tagToCheck = "Player";

    [HideInInspector]
    public OnTriggerEvent OnPlayerEvent;

    public void Awake()
    {
        if (OnPlayerEvent == null)
            OnPlayerEvent = new OnTriggerEvent();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(_tagToCheck))
            OnPlayerEvent.Invoke(other.gameObject);
    }


    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag(_tagToCheck))
            OnPlayerEvent.Invoke(other.gameObject);
    }
}
