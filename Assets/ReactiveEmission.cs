﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReactiveEmission : MonoBehaviour {

    [SerializeField] MeshRenderer _mesh;

    Color _baseColor;
    Material _mat;

	// Use this for initialization
	void Start () {
        _mat = Instantiate(_mesh.material);
        _baseColor = _mat.color;
        _mesh.material = _mat;
        GlobalPower.OnPowerChangeEvent.AddListener(UpdateIntensity);

	}
	
	void UpdateIntensity(float value)
    {
        _mat.color = _baseColor * value;
        _mesh.material = _mat;
    }

    private void OnDestroy()
    {
        GlobalPower.OnPowerChangeEvent.RemoveListener(UpdateIntensity);

    }
}
