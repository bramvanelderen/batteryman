﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntroLightFlash : MonoBehaviour
{

	private Light worldLight;
    private float regularIntensity;

	// Use this for initialization
	void Start ()
	{
        worldLight = GetComponent<Light>();
		StartCoroutine(Flash());
	}

//    private void Update()
//    {
//        light.intensity = Mathf.Sin(Time.time * 4) + 2;
//    }

    IEnumerator Flash()
	{
		float g = 1, b = 1;
		while (true)
		{
			while (worldLight.color.b > 5/255f)
			{
				worldLight.color = new Color(1, g, b);
				g -= 7.5f/255f;
				b -= 7.5f/255f;

				yield return new WaitForSeconds(0.02f);
			}
			while (worldLight.color.b < 250/255f)
			{

                worldLight.color = new Color(1, g, b);
				g += 7.5f/255f;
				b += 7.5f/255f;
				yield return new WaitForSeconds(0.02f);
			}
		}
		
	}
}
