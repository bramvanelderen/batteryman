﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class FactoryAnnouncement : MonoBehaviour {

    private static List<FactoryAnnouncement> _factories;
    private static List<FactoryAnnouncement> Factories
    {
        get
        {
            if (_factories == null)
                _factories = new List<FactoryAnnouncement>();

            return _factories;
        }
    }

    public static void GlobalAnnounce(string message, float duration)
    {
        foreach (var factory in Factories)
        {
            factory.Announce(message, duration);
        }
    }

    public static void GlobalAnnounce(string message, EnemyType type, float duration)
    {
        foreach (var factory in Factories)
        {
            factory.Announce(message, type, duration);
        }
    }

    public string preWrittenMessage = "";

    [SerializeField]
    private TextMeshPro _fullScreenText;
    [SerializeField]
    private TextMeshPro _smallText;
    [SerializeField]
    private Transform _container;
    [SerializeField]
    private List<Enemy> _enemyModels;

    private bool _isDisplayingBig = false;
    private bool _isDisplayingSmall = false;
    private Transform _activeTransform;
    private float _vel = 0;
    private float _vel1 = 0;

    private void Awake()
    {
        FactoryAnnouncement.Factories.Add(this);
    }
    // Use this for initialization
    void Start () {
        var color = _fullScreenText.color;
        color.a = 0;
        _fullScreenText.color = color;
        _smallText.color = color;
        foreach(var model in _enemyModels)
        {
            model.Obj.gameObject.SetActive(false);
        }

        if (preWrittenMessage != "")
        {
            Announce(preWrittenMessage, 9999999);
        }

	}
	
	// Update is called once per frame
	void Update () {

        

        _container.Rotate(Vector3.up * 30 * Time.deltaTime);

        /*TODO FIX
        foreach (var model in _enemyModels)
        {
            var scale = model.Obj.localScale;
            var vel = model.Vel;
            if (model.Obj == _activeTransform)
            {
                scale = Vector3.SmoothDamp(scale, new Vector3(1, 1, .01f), ref vel, .001f);
            }
            else
            {
                scale = Vector3.SmoothDamp(scale, new Vector3(0, 0, .01f), ref vel, .001f);
            }
            //model.Obj.localScale = scale;
        }
        */

        if (!_isDisplayingBig && _fullScreenText != null)
        {
            var color = _fullScreenText.color;
            color.a = Mathf.SmoothDamp(color.a, 0, ref _vel, .2f);
            _fullScreenText.color = color;
        }

        if (!_isDisplayingSmall && _smallText != null)
        {
            var color1 = _smallText.color;
            color1.a = Mathf.SmoothDamp(color1.a, 0, ref _vel1, .2f);
            _smallText.color = color1;
        }      



    }

    public void Announce(string message, float duration)
    {
        if (_fullScreenText == null)
            return;

        if (_smallText != null)
            _smallText.text = "";

        if (_fullScreenText != null)
            _fullScreenText.text = "";

        foreach (var model in _enemyModels)
        {
            model.Obj.gameObject.SetActive(false);
        }

        _isDisplayingBig = true;

        var color = _fullScreenText.color;
        color.a = 1;
        _fullScreenText.color = color;
        _fullScreenText.text = message;
        //CancelInvoke("RemoveMessage");
        //Invoke("RemoveMessage", duration);
    }

    public void Announce(string message, EnemyType type, float duration)
    {
        if (_smallText == null)
            return;

        if (_smallText != null)
            _smallText.text = "";

        if (_fullScreenText != null)
            _fullScreenText.text = "";

        foreach (var model in _enemyModels)
        {
            model.Obj.gameObject.SetActive(false);
        }

        _isDisplayingSmall = true;

        var color = _smallText.color;
        color.a = 1;
        _smallText.color = color;
        _smallText.text = message;

        _activeTransform = _enemyModels.Find(x => x.Type == type).Obj;
        _activeTransform.gameObject.SetActive(true);

        CancelInvoke("RemoveMessage");
        Invoke("RemoveMessage", duration);
    }

    public void RemoveMessage()
    {
        _isDisplayingBig = false;
        _isDisplayingSmall = false;
        _activeTransform = null;

        foreach (var model in _enemyModels)
        {
            model.Obj.gameObject.SetActive(false);
        }
    }

    [System.Serializable]
    class Enemy
    {
        public EnemyType Type;
        public Transform Obj;

        [HideInInspector]
        public Vector3 Vel;
    }
}

public enum EnemyType
{
    Basic,
    Big,
    Ranged,
    Explosive
}
