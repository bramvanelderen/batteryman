﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {

    [SerializeField] CustomClip roundComplete;
    [SerializeField] CustomClip levelComplete;
    private AudioSource roundAudioSource;
    private AudioSource levelAudioSource;
	private bool played = false;
	
	// Use this for initialization
	void Start () {
        LevelManager.OnRoundEnd.AddListener(OnRoundEndListener);
        LevelManager.OnLevelEnd.AddListener(OnLevelCompleteListener);
        roundAudioSource = CustomClip.CreateAudioSource(this);
        levelAudioSource = CustomClip.CreateAudioSource(this);
	}
	
    void OnRoundEndListener(Level level)
    {
        roundComplete.Play(roundAudioSource);
    }
	
	void OnLevelCompleteListener(Level level)
	{
		if (!played)
		{
			played = true;
			levelComplete.Play(levelAudioSource);
		}
	}
}
